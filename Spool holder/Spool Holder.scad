$fn = 120;

bearingR = 50;				//radius of the center of the bearing race
ballR = 2.5;				//radius of the steel balls (5mm diameter)
ballDepth = 2;				//depth of the ball inside the races
nBalls = 25;				//number of balls in the race


spacerThickness = 0.6;		//thickness of the ball spacer/cage
thickness = ballDepth+1;	//thickness of the races

edge = 10;					//space on the bearing plates from the conter of the race to the outside

PrintAll();

module PrintAll()
{
	distance = (bearingR+edge+5)*2;
	translate([0,0,0])
		topBearingPlate();
	translate([distance,0,0])
		lowerBearingPlate();
	translate([0,distance,0])
		bearingSpacer();
}

module lowerBearingPlate()
{
	difference()
	{
		linear_extrude(thickness)
			minkowski()
			{
				square([bearingR*2,bearingR*2],true);
				circle(edge);
			}
		rotate_extrude()
			translate([bearingR,thickness+ballR-ballDepth,0])
				circle(ballR);
		cylinder((thickness+2)*2,bearingR-edge,bearingR-edge,true);
	}
}

module bearingSpacer()
{
	linear_extrude(spacerThickness)
	{
		difference()
		{
			circle(bearingR+edge/2);
			circle(bearingR-edge/2);
			movement = 360/nBalls;
			for(i=[0:movement:360])
			{
				rotate([0,0,i])
					translate([bearingR,0,0])
						circle(ballR+0.3);
			}
		}			
	}
}

module topBearingPlate()
{
	difference()
	{
		linear_extrude(thickness)
		{
			difference()
			{
				circle(bearingR+edge);
				circle(bearingR-edge);
			}			
		}
		rotate_extrude()
			translate([bearingR,thickness+ballR-ballDepth,0])
				circle(ballR);
	}
}