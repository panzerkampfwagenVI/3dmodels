$fn = 30;


module screw(length)
{
	difference()	
	{
		intersection()	
		{
			cylinder(1.65,2.85,2.85);//head dymentions
			
			translate([0,0,-1.5])
			{
				sphere(3.5);
			}
		}
		translate([0,0,0.15])
		{
			cylinder(1.6,1,1,$fn=6);	
		}
	}
	translate([0,0,-length])
	{
		cylinder(length,1.5,1.5);
	}
}

