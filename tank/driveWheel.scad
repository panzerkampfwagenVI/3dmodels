include <hardware.scad>
include <track.scad>


$fn =30;

//all();
print();

module print()
{
	
	translate([0,-60,2])
	{
		rotate([90,0,0])
		{
		outerSide();
		}
	}
	rotate([90,0,0])
	{
	hub();
	}
	translate([0,60,2])
	{
		rotate([270,0,0])
		{
		innerSide();
		}
	}
}

module all()
{
	wheel();
	color([0.5,0.5,0.5])
	{	
		for(i=[0:24:180])
		{
			rotate([0,i,0])
			{
				translate([-3,0,-22.5])//inside is 21 and outside is 24
				{
					turnedTrack(24/2);
					//emulateLink(turn/2);
				}
			}
		}
		for(i=[0:1:10])
		{
			translate([i*9.5+6.5,0,-22.5])
			{
				track();
			}
		}
	}
	translate([0,100,0])
	{
		explodedView();	
	}
}

module explodedView()
{
	translate([0,30,0])
	{
		outerSide();
	}
	translate([0,50,0])
	{
		hub();
	}
	translate([0,110,0])
	{
		innerSide();
	}
		
	for(i=[0:60:360])
	{
		rotate([90,i,0])
		{
			translate([0,11,11])
			{
				screw(25);
				rotate([180,0,30])
				{
					translate([0,0,150])
					{
						bolt();
					}
				}
			}
		}
	}
}

module wheel()
{
	translate([0,-6,0])
	{
		outerSide();
	}
	hub();
	translate([0,6,0])
	{
		innerSide();
	}
		
	for(i=[0:60:360])
	{
		rotate([90,i,0])
		{
			translate([0,11,11])
			{
				screw(25);
				rotate([180,0,30])
				{
					translate([0,0,20])
					{
						bolt();
					}
				}
			}
		}
	}
}


module trackCircle()
{
	turn = 24;
	for(i=[0:turn:360])
	{
		rotate([0,i,0])
		{
			translate([-3,0,-22.5])//inside is 21 and outside is 24
			{
				//turnedTrack(turn/2);
				emulateLink(turn/2);
			}
		}
	}
}
module emulateLink(rotation)
{
	rotate([0,rotation,0])
	{
		hull()
		{
			rotate([90,0,0])
			{
				cylinder(2,1.5,1.5,true);
			}
			translate([-3.5,0,0])
			{	
				rotate([90,0,0])
				{
					cylinder(2,1.5,1.5,true);
				}
			}
		}
	}
}

			
module outerRing()
{
	difference()
	{
		union()
		{
			difference()
			{
				union()
				{
					rotate([90,0,0])
					{
						cylinder(10,21,21,true);//while wheel
					}
					translate([0,-5,0])
					{
						translate([0,-1.5,0])//ring with teeth to grip track	
						{	
							difference()
							{
								rotate([270,0,0],true)
								{
									minkowski()
									{
										cylinder(1,22.5,22);
										sphere(0.5);
									}
								}
								translate([0,.5,0])
								{
									trackCircle();
								}
							}
						}
					}
				}
				translate([0,-4,0])
				{
					rotate([90,0,0])
					{
						difference()
						{
							cylinder(10.01,7,15,true);
							for(i=[0:60:360])
							{
								rotate([0,0,i])
								{	
									translate([0,11,0])
									{
										cylinder(11,6,3.5,true);
									}
								}
							}
						}	
					}
				}
			}
		}
		translate([0,6,0])
		{
			hub();
		}
		for(i=[0:60:360])
		{
			rotate([90,i,0])
			{
				translate([0,11,0])
				{
					cylinder(15, 1.540,1.54,true);//shoeld be 1.5
				}
			}
		}
	}
}

module hub()
{
	difference()
	{
		union()
		{
			axialLegnth = 50;
			rotate([90,0,0])
			{
				cylinder(10,15,15,true);
			}
			translate([0,axialLegnth/2,0])
			{
				rotate([90,0,0])
				{
					cylinder(axialLegnth,4,4,true);
				}
			}
		}
		for(i=[0:60:360])
		{
			rotate([90,i,0])
			{
				translate([0,11,0])
				{
					cylinder(15, 1.5,1.5,true);
				}
			}
		}
	}
}

module outerSide()
{
	difference()
	{	
		outerRing();
		rotate([270,0,0])
		{
			translate([0,0,-7])
			{
				minkowski()
				{
					cylinder(2,17,15,true);
					sphere(1);
				}
			}
		}
	}
}

module innerSide()
{
	difference()
	{
		mirror([0,1,0])
		{
			outerSide();	
		}
		for(i=[30:60:360])
		{
			rotate([-90,i,0])
			{
				translate([11,0,3])
				{
					bolt();
				}
			}
		}
	}
}















