
amount = 10;

module all()
{
	tracklength = 9.5;
	for(i=[0:1:amount-1])
	{
		translate([i*tracklength,0,0])
		{
			track();
		}
	}
    translate([0,70,0])
    {
        explodedView();
    }
}

module explodedView()
{
    $fn = 60;
	
	translate([-3.5,-30,0.001])
	{
		lockPin();
	}	
	translate([0,-30,0.001])
	{
		lockPin();
	}	
	translate([-3.5,-15,0])
	{
		outerLink();
	}
	translate([0,-7,0])
	{
		pad();
	}
	translate([-3.5,0,0])
	{
		innerLink();
	}
	translate([0,7,0])
	{
		pad();
	}
	translate([-3.5,15,0])
	{
		outerLink();
	}
}

module track()
{
	$fn = 60;
	
	translate([-3.5,0,0])
	{
		lockPin();
	}	
    translate([0,0,0])
    {
        lockPin();
    }
	translate([-3.5,-12,0])
	{
		outerLink();
	}
	translate([0,-6,0])
	{
		pad();
	}
	translate([-3.5,0,0])
	{
		innerLink();
	}
	translate([0,6,0])
	{
		pad();
	}
	translate([-3.5,12,0])
	{
		outerLink();
	}
	
}


module turnedTrack(degrees)
{
	$fn = 60;
	rotate([0,degrees,0])
	{
		union()
		{
			translate([-3.5,0,0])
			{
				lockPin();
			}	
			translate([0,0,0])
			{
				lockPin();
			}
			translate([-3.5,-12,0])
			{
				outerLink();
			}
			translate([-3.5,0,0])
			{
				innerLink();
			}
			translate([-3.5,12,0])
			{
				outerLink();
			}
		}
	}
	translate([0,-6,0])
	{
		pad();
	}
	translate([0,6,0])
	{
		pad();
	}
	
}


module pad()
{
    difference()
    {
        union()
        {
            hull()
            {
                rotate([90,0,0])
                {
                    cylinder(10,1.5,1.5,true);
                }
                translate([6,0,0])
                {
                    rotate([90,0,0])
                    {
                        cylinder(10,1.5,1.5,true);
                    }
                }
            }
            space = 2;
            for(i=[0:1:2])
            {
                translate([i*space+1,0,-1.75])
                {
                    hull()
                    {
                        translate([0,-4,0])
                        {
                            cylinder(0.5,0.5,0.5, true);
                        }
                        translate([0,4,0])
                        {
                            cylinder(0.5,0.5,0.5, true);
                        }
                    }
                }
            }
        }
        lockPin();
        translate([6,0,0])
        {
            lockPin();
        }
    }
}

module outerLink()
{
    difference()
    {
        hull()
        {
            rotate([90,0,0])
			{
				cylinder(2,1.5,1.5,true);
			}
            translate([3.5,0,0])
            {
                rotate([90,0,0])
                {
                    cylinder(2,1.5,1.5,true);
                }
            }
        }
        
        lockPin();
        translate([3.5,0,0])
        {
            lockPin();
        }
    }

}

module innerLink()
{
	outerLink();
    difference()
    {
        translate([1.75,0,0])
        {
            hull()
            {
                translate([0,0,5])		//hight = y-1
                {
                    rotate([0,90,0])
                    {
                        cylinder(1.5,0.5,0.5,true);
                    }
                }
                rotate([0,90,0])
                {
                    cylinder(3,1,1,true);
                }
            }
        }
        lockPin();
        translate([3.5,0,0])
        {
            lockPin();
        }
    }
}