include <track.scad>
include <screw.scad>
include <bearing.scad>

all();

$fn = 60;

bearingIR = 4;
bearingOR = 7;
beartingWidth = 4;

wheelOR = 20;

module all()
{
	translate([50,0,23])
	{
		rotate([90,0,0])
		{
			wheel();
		}
	}	
	for(i=[0:1:10])
	{
		translate([i*9.5,0,1.5])
		{
			track();
		}
	}
	translate([0,100,0])
	{
		rotate([90,0,0])
		{
			explodedView();
		}
	}
	
}

module explodedView()
{
	translate([0,0,60])
	{
		for(i=[0:60:360])
		{
			rotate([0,0,i])
			{
				translate([11,0,0])
				{
					bolt();
				}
			}
		}
	}
	translate([0,0,30])
	{
		rotate([180,0,0])
		{
			innerSide();	
		}
	}
	translate([0,0,15])
	{
		bearing();
	}
	hub();
	translate([0,0,-15])
	{
		bearing();
	}
	translate([0,0,-30])
	{
		outerSide();
	}
	translate([0,0,-80])
	{
		for(i=[0:60:360])
		{
			rotate([0,0,i])
			{
				translate([11,0,0])
				{
					rotate([180,0,0])
					{
						screw(20);	
					}
				}
			}
		}
	}
}

module wheel()
{
	translate([0,0,6.5])
	{
		for(i=[0:60:360])
		{
			rotate([0,0,i])
			{
				translate([11,0,0])
				{
					bolt();
				}
			}
		}
	}
	translate([0,0,5.75])
	{
		rotate([180,0,0])
		{
			innerSide();	
		}
	}
	translate([0,0,3])
	{
		bearing();
	}
	hub();
	translate([0,0,-3])
	{
		bearing();
	}
	translate([0,0,-5.75])
	{
		outerSide();
	}
	translate([0,0,-5])
	{
		for(i=[0:60:360])
		{
			rotate([0,0,i])
			{
				translate([11,0,0])
				{
					rotate([180,0,0])
					{
						difference()
						{
							screw(20);
							translate([0,0,-22])
							{
								cylinder(6.1,2,2);
							}	
						}
					}
				}
			}
		}
	}
}

module hub()
{
	width = 10;
	difference()
	{
		cylinder(width,15,15,true);
		for(i=[0:60:360])
		{
			rotate([0,0,i])
			{
				translate([11,0,0])
				{
					cylinder(width+1,1.5,1.5,true);
				}
			}
		}
		cylinder(width+1,bearingIR,bearingIR,true);
		translate([0,0,3])
		{
			cylinder(beartingWidth+0.001,bearingOR,bearingOR,true);
		}
		translate([0,0,-3])
		{
			cylinder(beartingWidth+0.001,bearingOR,bearingOR,true);
		}
	}
	
}

module outerSide()
{
	difference()
	{
		union()
		{
			difference()
			{
				cylinder(8.5,20,20, true);//base wheel
				translate([0,0,-2.75])
				{
					minkowski()	//front
					{
						cylinder(3,15,14,true);
						sphere(1);
					}
				}
				
				for(i=[0:60:360]) //boltholes
				{
					rotate([0,0,i])
					{
						translate([11,0,0])
						{
							cylinder(11,1.5,1.5,true);
						}
					}
				}
			}
				
			for(i=[30:60:360])//spokes
			{
				rotate([0,0,i])
				{
					minkowski()
					{
						hull()
						{
							translate([18,0,0])
							{
								cube([1,2,5],true);
							}
							cube([1,0.1,0.1],true);
						}	
						sphere(1);
					}
				}
			}
			translate([0,0,4])//round front
			{
				difference()
				{			
					sphere(8);
				}
			}
		}
		
		translate([0,0,6.75])//read
		{
			cylinder(12,15,15,true);
			cylinder(13,bearingIR,bearingIR,true);
		}
	}
}

module innerSide()
{
	difference()
	{
		union()
		{
			cylinder(8.5,20,20, true);
		}	
		translate([0,0,6.75])//read
		{
			cylinder(12,15,15,true);
		}
		cylinder(13,bearingIR,bearingIR,true);
		
		for(i=[0:60:360]) //boltholes
		{
			rotate([0,0,i])
			{
				translate([11,0,0])
				{
					cylinder(11,1.5,1.5,true);
					translate([0,0,-0.75])
					{
						rotate([180,0,0])
						{
							bolt();
						}
					}
				}
			}
		}
		translate([0,0,-4.25])
		{
			minkowski()
			{
				cylinder(1,15,14);
				sphere(1);
			}
			
		}
	}
}






	







