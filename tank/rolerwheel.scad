wheel();

module wheel()
{
	difference()
	{
		translate([7,0,0])
		{
			square([14,10],true);
		}
		translate([3.5,0,0])
		{
			square([7,4],true);
		}
		translate([11.5,0,0])
		{
			square([5,3],true);
		}
	}
}