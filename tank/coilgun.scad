$fn = 60;

barrelLength = 300;
barrelIR = 2.75;
barrelOR = 3.5;

spoolOR = 10;
spoolWasherThickness = 2;

printOptions();

module printOptions()
{
	barrel();
	
	//spoolWasher();
}

module barrel()
{
	linear_extrude(barrelLength)
		difference()
		{
			circle(barrelOR);
			circle(barrelIR);
		}
}

module spoolWasher()
{
	linear_extrude(spoolWasherThickness)
		difference()
		{
			circle(spoolOR);
			circle(barrelOR);
		}
}