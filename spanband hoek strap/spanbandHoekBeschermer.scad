$fn = 60;

rotate([90,315,0])
	cornerStrap();

module cornerStrap()
{
	halfCornerStrap();
	translate([0,0,66])
	mirror([0,0,1])
		halfCornerStrap();
}

module halfCornerStrap()
{
	inside = 50;
	guildHight = 15;
	
	outsideStrap = 65;
	outsideGuild = 70;
	outsideSmall = 60;
	difference()
	{
		union()
		{
			hull()
			{
				linear_extrude(10)
					cornerOutside(outsideSmall);
				translate([0,0,guildHight])
					linear_extrude(5)
						cornerOutside(outsideGuild);
			}
			translate([0,0,guildHight+5])
			linear_extrude(13)
				cornerOutside(outsideStrap);
		}
		linear_extrude(33)
			cornerInside(50);
	}
}

module cornerOutside(outside)
{
	offset(14)
		offset(-14)
			square(outside);
}

module cornerInside(inside)
{
	square(inside);
	translate([inside-3,inside-3])
		circle(5);
}