
   
tank();
//ground();


fenderwidth = 3;    
hullwidth = 11;
hullhight = 6.5;

skirtwith = 0.2;
trackthickness = 0.3;

module ground()
{   
    translate([-500,-500,-0.11])
    {
        color([0,1,1,0.7])
        {
            cube([1000,1000,0.1]);
        }   
    }  
}

module tank()
{
translate([0, fenderwidth+skirtwith, 2.3]){hul();}
translate([0,fenderwidth/2+skirtwith+1,0]){color([1,0,0]){suspention();}}
translate([0,fenderwidth*1.5+skirtwith+1+hullwidth-2,0]){color([1,0,0]){mirror([0,1,0])suspention();}}
translate([15,(hullwidth/2)+fenderwidth+skirtwith,hullhight+2.4]){color([1,0,0]){turret();}}
}

module hul()
{   
color([0,0,1]){romp();}
translate([0,-fenderwidth,0]){fender();} //fenders
translate([0,hullwidth,0]){fender();}

translate([0,-(fenderwidth+skirtwith),0]){sideskirt();} //sideskirts
translate([0,(fenderwidth+hullwidth),0]){sideskirt();}  

translate([15,(hullwidth/2),6.5]){color([0,0,0]){turretring();}}
}

module romp()
{           //0         1         2         3         4         5
    points = [[5,0,0], [29,0,0], [32,0,3.5], [31,0,5], [25,0,hullhight], [0,0,hullhight],[0,0,5],[2,0,1],
              [5,hullwidth,0], [29,hullwidth,0], [32,hullwidth,3.5], [31,hullwidth,5], 
			  [25,hullwidth,hullhight], [0,hullwidth,hullhight], [0,hullwidth,5], [2,hullwidth,1]];
    faces =  [[0,1,2,3,4,5,6,7],[8,9,10,11,12,13,14,15],
              [0,1,9,8],[1,2,10,9],[2,3,11,10],[3,4,12,11],[4,5,13,12],[5,6,14,13],[6,7,15,14],[7,0,8,15]];
    polyhedron(points,faces,10);
}
module fender()
{
    points = [[0,0,5],[31,0,5],[25,0,hullhight],[0,0,hullhight],
              [0,fenderwidth,5],[31,fenderwidth,5],[25,fenderwidth,hullhight],[0,fenderwidth,hullhight]];
    faces = [[0,1,2,3], [4,5,6,7],
             [0,1,5,4], [1,2,6,5], [2,3,7,6], [3,0,4,7]];
    polyhedron(points,faces);
}

module sideskirt()
{
    points = [[0,0,3],[31,0,3],[31,0,5],[25,0,hullhight],[0,0,hullhight],
              [0,skirtwith,3],[31,skirtwith,3],[31,skirtwith,5],[25,skirtwith,hullhight],[0,skirtwith,hullhight]];
    faces = [[0,1,2,3,4], [5,6,7,8,9],
             [0,1,6,5], [1,2,7,6], [2,3,8,7], [3,4,9,8], [4,0,5,9]];
    polyhedron(points,faces);
}

module turretring()
{
    $fn = 360;
    difference()
    {
        cylinder(0.4,6,6, true);
        cylinder(1,5.9,5.9, true);
    }
}

module suspention()
{
    $fn = 360;
    translate([30,0,5])
    {
        rotate([90,0,0])
        {
            frontWheel();
        }   
    }
	#for(i=[0:1:4])
	{
		position = 7+(4.7*i);
		translate([position,0,2.3])
		{
			rotate([90,0,0])
			{
				roadWheel();
			}
			translate([0,-1.90,0])
			{
				rotate([0,-20,0])
				{
					suspencionArm();
				}
			}
		}
	}
    translate([3,0,4.8])
    {
        rotate([90,0,0])
        {
            driveWheel();
        }   
    }
    
    tracks();   
}

module tracks()
{
    
}

module suspencionArm()
{
	rotate([270,0,0])
	{
		cylinder(3.6,0.4,0.4); //1.9 in wiel 2.4 tot romp 
	}
	translate([0,3.2,0])
	{
		rotate([0,90,0])
		{
			cylinder(3,0.4,0.4);
		}
		translate([3,0,0])
		{
			rotate([90,0,0])
			{
				cylinder(0.4,1,1,true);//bearing hull
			}
		}
	}
}

module frontWheel()
{
    cylinder(2,1.8,1.8);
	rotate([0,180,0])
	{
		cylinder(4,0.4,0.4);
	}
} 

module roadWheel()
{
    cylinder(2,2,2);
}

module driveWheel()
{
    cylinder(2,2,2);
	rotate([0,180,0])
	{
		cylinder(4,0.4,0.4);
	}
}

module turret()
{
    turretHight = 3.5;
    points =    [[-10,4,0],[0,7,0],[4,6,0],[7,1,0],     //right bottom
                [-10,-4,0],[0,-7,0],[4,-6,0],[7,-1,0],   //left bottom
                [7.5,4,1.5],[8.5,1.5,1.5],[8.5,-1.5,1.5],[7.5,-4,1.5], //front side
                [-10,2.8,turretHight-0.2],[0,5.5,turretHight+0.3],  // right side top
                [3,4.8,turretHight],[4,1.5,turretHight],
                [-10,-2.8,turretHight-0.2],[0,-5.5,turretHight+0.3],//left side top
                [3,-4.8,turretHight],[4,-1.5,turretHight]];
    
    faces =     [[0,1,2,3,7,6,5,4],             //bottom
                [12,13,14,15,19,18,17,16],      //top
                [2,3,8],[8,3,9],[3,9,10,7],[10,7,11],[6,7,11],    //front bottom
                [14,15,8],[8,15,9],[15,9,10,19],[19,18,11,10],[19,18,11],//front top
                [0,12,13,1],[1,13,14,8,2],      //side left
                [4,5,17,16],[5,6,11,18,17],     //side right
                [0,4,16,12]];
    
    polyhedron(points,faces);
    color(0,0,0)
    {
        translate([6,0,1.5]){ rotate([0,90,0]){ gun();}}
    }
}

module gun()
{
    $fn = 30;
    difference()
    {
        cylinder(20,0.8,0.7);
        cylinder(21,0.5,0.5);
    }
}





















