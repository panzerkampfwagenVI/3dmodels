$fn = 60;

//dimentions to interface with
mountingSurfaceThickness = 30; //mm thickness of the mounting surface
surfaceRounded = true; //true if the mounting surface has a rounded end on the side paralel to the bar
barDistance = 60; //mm distance between the mounting surface and the bar wich hold the roles
barR = 6.5; //mm radius of the bar on wich the rols wil be positioned. ! radius = diameter/2 !

//dimentions of the part
cutoutScale = 10; //mm about the thickness of the beams (referenceable on the X and Y plane)
width = 20; //mm width of each side (referenceable to the Z axis in preview)
depth = 110; //mm total depth (referenceable to the X axis) 
barOffset = -55; //mm distance from the front of the mount 
					//at 0 the barwill be 

printMount();

module printMount()
{
	endMount();
}

module endMount()
{
	basicMount();
}

module middleMount()
{
	
}

//base form for middle and end mount
module basicMount()
{
	linear_extrude(width)
	{
		if(surfaceRounded)
		{
			OD = cutoutScale*2+mountingSurfaceThickness;
			translate(-[OD,0])
			difference()
			{
				hull()
				{
					polygon([[cutoutScale/5*4,0],[OD,depth],[OD,0]]);
					translate([OD/2,depth-OD/2])
					circle((OD)/2);
				}
				
					hull()
					{
						translate([cutoutScale,0])
						square([mountingSurfaceThickness,0.1]);
						translate([OD/2,depth-OD/2])
						circle(mountingSurfaceThickness/2);
					}
			}
		}
		else
		{
			
		}
		
		difference()
		{
			hull()
			{
				square([0.1,depth]);
				translate([barDistance,depth+barOffset,0])
					circle(barR+cutoutScale);
			}
			translate([0,cutoutScale])
				hull()
				{
					square([0.1,depth-cutoutScale*2]);
					translate([barDistance,depth+barOffset-cutoutScale,0])
						circle(barR);
				}
		}
		translate([barDistance,depth+barOffset,0])
			difference()
			{
				circle(barR+cutoutScale);
				circle(barR);
			}
	}
}

