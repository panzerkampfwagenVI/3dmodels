                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2841200
Tiger Tank Breakdown by chanson836 is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

04/07/2018 Update -
Thanks to RustyBuckets for pointing out the error in the Tracks.  I have updated this with two new tracks.

This is a breakdown of m_bergman's excellent 100 scale Tiger I tank. By separating the tracks, upper and lower hull, it allows you to use minimal support that is easy to remove.
The turret is split also. Lay each piece flat to print. The bottom hull should be printed upside-down.

# Print Settings

Printer Brand: Wanhao
Printer: Duplicator 6
Rafts: No
Supports: Yes
Resolution: .1 mm
Infill: 30%

Notes: 
Lay each piece flat and use support from Base. Bottom hull should be printed upside down.

Support % for the hull and tracks can be pretty low (15-20%). However, you may want to use a higher % for the turret top (30-35%) to make sure that it is supported properly.