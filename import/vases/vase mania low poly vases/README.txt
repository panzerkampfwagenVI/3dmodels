                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2638924
Vasemania: Low poly vases by Ferjerez is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

Hi!

This another **Vase generator** made in **OpenSCAD**.

This one creates **low-poly** vases with optional *spikes*.

I encourage you to play with  the customizer or download and tweak the OpenSCAD file and find your nicest vase.

Also included **a ZIP file with 50 STL's ready to print**

You may find interesting my others vase-generators [here](https://www.thingiverse.com/thing:2581807) and [here](https://www.thingiverse.com/thing:2537095)

Hope you find it useful.
Have fun!!

Models in pictures printed using the Anet E10 and the Creality CR-10
You can buy printers here (affiliate links):
<a href="http://www.gearbest.com/3d-printers-3d-printer-kits-c_11399/?lkid=10060570">Printers on Gearbest.com</a>


# Print Settings

Printer: ANET E10 + Creality CR-10
Rafts: No
Supports: No
Resolution: 0.2

Notes: 
<p>Print in Spiral-Vase mode.</p>
<p>
Tips: Increase flow, reduce speed and use 3 or 4 bottoms.

Another tip: If you have a small nozzle (0.3 or 0.4mms) can increase the 'wall width' to 0.5 or 0.6 to get a stronger surface
</p>

# Thumbnails for the STLs in ZIP file

![Alt text](https://cdn.thingiverse.com/assets/3e/32/74/7f/60/50vases_1.jpg)

![Alt text](https://cdn.thingiverse.com/assets/8a/35/d0/e9/9b/50vases_2.jpg)