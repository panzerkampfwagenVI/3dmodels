                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:1479590
Model of a "Dutch" Windmill for 1:100 (15mm/FoW) or 1:160 (N) Scales by wisar is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

NOTE!!!  There was an error in the initial upload of this file!  The Sail.STL file had a bunch on internal faces that may confuse your slicer.   The file has been corrected now.

More on this project can be found on my Blog and on the Instructable that covers assembly:

Where the idea came from:
http://raspberrypirobot.blogspot.co.uk/2016/03/designing-3d-printable-model-of-dutch.html

How to build it:
http://www.instructables.com/id/Building-a-3D-Printed-Model-of-a-Dutch-Windmill-in/

# Print Settings

Printer Brand: Ultimaker
Printer: Ultimaker 2
Rafts: No
Supports: Yes
Resolution: 100-200 microns

Notes: 
The 1:100 scale version of this model can be printed with a standard size nozzle (unlike some of my work).   If you want to reduce the size of the print for 1:160, or N, scale you will need a smaller than standard nozzle for quality of things like the sails, railings, and braces.  You can try to reduce extrusion width to print in a smaller scale but quality will suffer.

Support is only needed for the top piece and only for the sails axel.  The Tower can actually benefit from being printed at 200 microns as the grain can be painted and will sorta look like wood.   Other parts are up to you!