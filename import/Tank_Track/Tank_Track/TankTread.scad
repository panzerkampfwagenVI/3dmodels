// filename: TankTread.scad
//  by Dave Flynn 2013
// This file contains the track parts for "The Tank"

include <CommonStuff.scad>

//viewTrackSection();	// Shows a section of track in colors

// ***** Routines for STL output *****

//EndPinSTL();			// This pin attaches the outer link to the tread top, print 640
//treadTopSTL();			// Main tread part that everything bolts to, print 160
//treadBottomSTL();		// The grip pad, print 320
//outerLinkSTL();		// Connects the treads to each other, print 320
//MidLinkSTL();			// Connects the treads to each other and centers the tread on the rollers, print 160

// ***********************************


module EndPin(){
	// Printed OK 11/25/12

	rotate(a=[0,90,0]) difference(){
		union(){
			cylinder(r=rPin,h=lLinkWidth);
			translate([0,0,lLinkWidth]) cylinder(r=rPin-0.5,h=1);
			cylinder(r=rPin+rPinFlange,h=1);
		} // union

		translate([0,0,-0.1]) cylinder(r=rEndPinBoltHead,h=hEndPinBoltHead);
		//translate([0,0,hEndPinBoltHead-0.4]) cylinder(r1=rEndPinBoltHead,r2=rEndPinBoltClear,h=0.5);
		cylinder(r=rEndPinBoltClear,h=rPin*4);
	} // diff
} // EndPin

module EndPinSTL(){
	rotate (a=[0,-90,0]) EndPin();
} // EndPinSTL

module EndPinBoss(){
	rotate(a=[0,90,0]) translate([0,0,lLinkWidth-0.01]) cylinder(r=rPin-0.25,h=1);
} // EndPinBoss

module treadTop(){
	// 11/28/12 Rounded ends

	// first pad
	difference(){
		union(){
			// Left Top Pad
			translate([lLinkWidth,0,-rPin*1.25])
				cube([lTreadWidth,lLinkCenter,rPin*2.5]);
			translate([lLinkWidth,-rPin*1.25,-rPin*1.25])
				cube([lTreadWidth,lLinkCenter+rPin*2.5,rPin*1.25]);
			translate([lLinkWidth+lTreadWidth/2,0,0]) rotate(a=[0,90,0])
				cylinder(r=rPin*1.25,h=lTreadWidth,center=true);
			translate([lLinkWidth+lTreadWidth/2,lLinkCenter,0]) rotate(a=[0,90,0])
				cylinder(r=rPin*1.25,h=lTreadWidth,center=true);
		} // union

		// link pin bosses
		EndPinBoss(); rotate(a=[0,90,0]) cylinder(r=rEndPinBolt,h=rPin*5);
		translate([0,lLinkCenter,0]) { EndPinBoss(); rotate(a=[0,90,0])
			cylinder(r=rEndPinBolt,h=rPin*5);}

		// link pin bolt holes
		translate([lLinkWidth+lTreadWidth/3,lLinkCenter/2,rPin]) BoltHeadHole();
		translate([lLinkWidth+2*lTreadWidth/3,lLinkCenter/2,rPin]) BoltHeadHole();

	} // diff

	// Second pad
	difference(){
		union(){
			// Right Top Pad
			translate([lTrackWidth-lLinkWidth,0,rPin*1.25]) rotate(a=[0,180,0])
				cube([lTreadWidth,lLinkCenter,rPin*2.5]);
			translate([lTrackWidth-lLinkWidth,-rPin*1.25,0]) rotate(a=[0,180,0])
				cube([lTreadWidth,lLinkCenter+rPin*2.5,rPin*1.25]);
			translate([lTrackWidth-(lLinkWidth+lTreadWidth/2),0,0]) rotate(a=[0,90,0])
				cylinder(r=rPin*1.25,h=lTreadWidth,center=true);
			translate([lTrackWidth-(lLinkWidth+lTreadWidth/2),lLinkCenter,0]) rotate(a=[0,90,0])
				cylinder(r=rPin*1.25,h=lTreadWidth,center=true);
		
		} // union

		// link pin Bosses
		translate([lTrackWidth,lLinkCenter,0]) rotate(a=[0,180,0]) { 
			EndPinBoss(); rotate(a=[0,90,0]) cylinder(r=rEndPinBolt,h=rPin*5);}
		translate([lTrackWidth,0,0]) rotate(a=[0,180,0]) { 
			EndPinBoss(); rotate(a=[0,90,0]) cylinder(r=rEndPinBolt,h=rPin*5);}

		// link pin bolt holes
		translate([lTrackWidth-(lLinkWidth+2*lTreadWidth/3),lLinkCenter/2,rPin]) BoltHeadHole();
		translate([lTrackWidth-(lLinkWidth+lTreadWidth/3),lLinkCenter/2,rPin]) BoltHeadHole();

	} // diff

	//Center Pins
	translate([lTrackWidth/2-lCenterLinkWidth/2,0,0]) rotate(a=[0,90,0])
		cylinder(r=rPin,h=lCenterLinkWidth);
	translate([lTrackWidth/2-lCenterLinkWidth/2,lLinkCenter,0]) rotate(a=[0,90,0])
		cylinder(r=rPin,h=lCenterLinkWidth);

} // treadTop

//treadTop(); // for testing olny

module treadTopSTL(){
	translate([-lTrackWidth/2,-lLinkCenter/2,rPin*1.25]) treadTop();
} // treadTopSTL

	lBTreadThickness=rPin*0.5;
	lGripPad=rPin;
	rGripPad=rPin*0.75;

module OneTreadBottom(){
	difference(){
		union(){
			// Left Bottom Pad
			hull(){
				translate([rPin*1.25,0,0])
					cylinder(r=rGripPad,h=lBTreadThickness);
				translate([lTreadWidth-rPin*1.25,0,0])
					cylinder(r=rGripPad,h=lBTreadThickness);
				translate([rPin*1.25,lLinkCenter,0])
					cylinder(r=rGripPad,h=lBTreadThickness);
				translate([lTreadWidth-rPin*1.25,lLinkCenter,0])
					cylinder(r=rGripPad,h=lBTreadThickness);
			} // hull

		// grip pad
		hull(){
			translate([rPin*1.25,1,-lGripPad])
				cylinder(r=rGripPad,h=lGripPad);
			translate([lTreadWidth-rPin*1.25,1,-lGripPad])
				cylinder(r=rGripPad,h=lGripPad);
		} // hull

		// grip pad
		hull(){
			translate([rPin*1.25,lLinkCenter/2,-lGripPad])
				cylinder(r=rGripPad,h=lGripPad);
			translate([lTreadWidth-rPin*1.25,lLinkCenter/2,-lGripPad])
				cylinder(r=rGripPad,h=lGripPad);
		} //hull

		// grip pad	
		hull(){
			translate([rPin*1.25,lLinkCenter-1,-lGripPad])
				cylinder(r=rGripPad,h=lGripPad);
			translate([lTreadWidth-rPin*1.25,lLinkCenter-1,-lGripPad])
				cylinder(r=rGripPad,h=lGripPad);
		} // hull


		} // union

		// bolt holes
		translate([lTreadWidth/3,lLinkCenter/2,lBTreadThickness])
			BoltHole(depth=lBTreadThickness+lGripPad-1);
		translate([2*lTreadWidth/3,lLinkCenter/2,lBTreadThickness])
			BoltHole(depth=lBTreadThickness+lGripPad-1);
	
	} // diff

} // OneTreadBottom


module treadBottom(){

	translate([lLinkWidth,0,-lBTreadThickness-rPin*1.25]) OneTreadBottom();
	translate([lTrackWidth-(lLinkWidth+lTreadWidth),0,-lBTreadThickness-rPin*1.25]) OneTreadBottom();

} // treadBottom

//treadBottom(); // for testing

module treadBottomSTL(){
	translate([-lTrackWidth/2,lLinkCenter/2,-rPin*1.25]) rotate(a=[180,0,0]) treadBottom();
} // treadBottomSTL

module outerLink(){
	// Printed 11/25/12 too large, but usable

	rPinClearHole=rPin+rIDInc+0.2; // + loose fit 
	rPinFlangeClearHole=rPin+rPinFlange+rIDInc+0.2;  // + loose fit 
	rLinkOD=rPin*1.74;
	

	difference(){
		union(){
			rotate(a=[0,90,0]) cylinder(r=rLinkOD,h=lLinkWidth-0.25);
			translate([0,-rPin*3,0]) rotate(a=[0,90,0]) cylinder(r=rLinkOD,h=lLinkWidth-0.25);
			translate([lLinkWidth/2,-rPin*1.75,0]) cube([lLinkWidth-0.5,rPin*3,rPin*2.5],center=true);
		} // union

		translate([-0.125,0,0]) rotate(a=[0,90,0]) {
			cylinder(r=rPinClearHole,h=lLinkWidth);
			cylinder(r=rPinFlangeClearHole,h=1.25);}
		
		translate([-0.125,-rPin*3,0]) rotate(a=[0,90,0]){
			cylinder(r=rPinClearHole,h=lLinkWidth);
			cylinder(r=rPinFlangeClearHole,h=1.25);}
	} // diff
	
} // outerLink

//outerLink(); // for testing

module outerLinkSTL(){
	rotate (a=[0,90,0]) translate([-lLinkWidth+0.25,0,0]) outerLink();
} // outerLinkSTL

module middleLink(stlOut=false){

	translate([lTrackWidth/2-lCenterLinkWidth/2+0.25,0,0]) difference(){
		union() {
			// near cylinder
			rotate(a=[0,90,0]) cylinder(r=rPin*1.74,h=lCenterLinkWidth-0.5);

			// far cylinder
			translate([0,-rPin*3,0]) rotate(a=[0,90,0])
				cylinder(r=rPin*1.74,h=lCenterLinkWidth-0.5);
			
			// center block
			translate([lCenterLinkWidth/2-0.25,-lCenterLinkWidth/2,0]) hull(){
				translate([0,rPin*3,0]) cylinder(r=rPin*1.375,h=rPin*2.75, center=true);
				translate([0,-rPin*3,0]) cylinder(r=rPin*1.375,h=rPin*2.75, center=true);}

			// alignment fin
			hull(){
				translate([lCenterLinkWidth/2-0.25,-rPin*1.5,rPin*4]) rotate(a=[0,90,0])
					cylinder(r=rPin,h=rPin, center=true);
				translate([lCenterLinkWidth/2-0.25,-rPin*1.5,rPin]) rotate(a=[0,90,0])
					cylinder(r=rPin*1.5,h=rPin*1.5, center=true);
			} // hull
		
		} // union

		if (stlOut){
			// near cylinder bore
			translate([0.3,0,0]) rotate(a=[0,90,0]) cylinder(r=rPin+rIDInc,h=rPin*2.75-0.6);
			translate([0,0,0]) rotate(a=[0,90,0]) cylinder(r=1,h=rPin*3);

			// far cylinder bore
			translate([0.3,-rPin*3,0]) rotate(a=[0,90,0]) cylinder(r=rPin+rIDInc,h=rPin*2.75-0.6);
			translate([0,-rPin*3,0]) rotate(a=[0,90,0]) cylinder(r=1,h=rPin*3);}
		else{
			// near cylinder bore
			translate([-0.25,0,0]) rotate(a=[0,90,0]) cylinder(r=rPin+rIDInc,h=rPin*3);
			// far cylinder bore
			translate([-0.25,-rPin*3,0]) rotate(a=[0,90,0]) cylinder(r=rPin+rIDInc,h=rPin*3);
		} // if


				//bolt holes
			translate([lCenterLinkWidth/2-0.25,rPin*1.75,0]) cylinder(r=rEndPinBolt,h=rPin*2);
			translate([lCenterLinkWidth/2-0.25,-rPin*4.75,0]) cylinder(r=rEndPinBolt,h=rPin*2);

			translate([lCenterLinkWidth/2-0.25,rPin*1.75,0]) rotate(a=[180,0,0])
				cylinder(r=rEndPinBoltClear,h=rPin*2);
			translate([lCenterLinkWidth/2-0.25,-rPin*4.75,0]) rotate(a=[180,0,0])
				cylinder(r=rEndPinBoltClear,h=rPin*2);
			
			// bolt heads
			translate([lCenterLinkWidth/2-0.25,rPin*1.75,-rPin*0.75]) rotate(a=[180,0,0])
				cylinder(r=rEndPinBoltHead,h=hEndPinBoltHead);
			translate([lCenterLinkWidth/2-0.25,-rPin*4.75,-rPin*0.75]) rotate(a=[180,0,0])
				cylinder(r=rEndPinBoltHead,h=hEndPinBoltHead);
		
	} // diff
} // middleLink

//middleLink();

module MidLinkSTL(){
	xOffset=-50;

	// Top half
	difference(){
		translate([xOffset,0,0]) middleLink(true);
		translate([xOffset,lTrackWidth/2,0]) rotate(a=[180,0,0])
			cube([lTrackWidth,lTrackWidth,lTrackWidth]);
	} // diff

	// Bottom half
	difference(){
		translate([xOffset-lCenterLinkWidth-4,-rPin*3,0]) rotate(a=[180,0,0]) middleLink(true);
		translate([xOffset,lTrackWidth/2,0]) rotate(a=[180,0,0])
			cube([lTrackWidth,lTrackWidth,lTrackWidth]);
	} // diff

	// Raft to keep it stuck to the table
	translate([-15,-26,0]) cube([35,40,0.3]);
} // MidLinkSTL


module viewTrackSection(){
	 
	color("Tan"){
	middleLink();
	outerLink();
	translate([lTrackWidth,-rPin*3,0]) rotate(a=[0,0,180]) outerLink();}
	treadTop();
	color("Lime"){
	EndPin(); translate([lTrackWidth,0,0]) rotate(a=[0,0,180]) EndPin();
	translate([0,lLinkCenter,0]) { EndPin(); translate([lTrackWidth,0,0]) rotate(a=[0,0,180]) EndPin();}}
	color("Brown") treadBottom();


	translate([0,lTreadPitch,0]) {
		color("Tan"){
		outerLink();
		middleLink();
		translate([lTrackWidth-lLinkWidth,0,0]) outerLink();}
		treadTop();
		color("Lime"){
		EndPin(); translate([lTrackWidth,0,0]) rotate(a=[0,0,180]) EndPin();
		translate([0,lLinkCenter,0]) { EndPin(); translate([lTrackWidth,0,0]) rotate(a=[0,0,180]) EndPin();}}
		color("Brown") treadBottom();
		}

	translate([0,lTreadPitch*2,0]) {
		color("Tan"){
		outerLink();
		middleLink();
		translate([lTrackWidth-lLinkWidth,0,0]) outerLink();}
		//treadTop();
		//color("Lime"){
		//EndPin(); translate([lTrackWidth,0,0]) rotate(a=[0,0,180]) EndPin();
		//translate([0,lLinkCenter,0]) { EndPin(); translate([lTrackWidth,0,0]) rotate(a=[0,0,180]) EndPin();}}
		color("Brown") treadBottom();
		}

	translate([0,-lTreadPitch,0]) {
		treadTop();
		color("Tan"){ outerLink(); middleLink();
		translate([lTrackWidth-lLinkWidth,0,0]) outerLink();}
		treadTop();
		color("Lime"){
		EndPin(); translate([lTrackWidth,0,0]) rotate(a=[0,0,180]) EndPin();
		translate([0,lLinkCenter,0]) { EndPin(); translate([lTrackWidth,0,0]) rotate(a=[0,0,180]) EndPin();}}
		color("Brown") treadBottom();
		}

	translate([0,-lTreadPitch*2,0]) {
		//treadTop();
		//color("Tan"){ outerLink(); middleLink();
		//translate([lTrackWidth-lLinkWidth,0,0]) outerLink();}
		treadTop();
		color("Lime"){
		//EndPin(); translate([lTrackWidth,0,0]) rotate(a=[0,0,180]) EndPin();}
		translate([0,lLinkCenter,0]) { EndPin(); translate([lTrackWidth,0,0]) rotate(a=[0,0,180]) EndPin();}}
		//color("Brown") treadBottom();
		}

} // viewTrackSection




$fn=50;



