$fn = 30;

width = 52;
length = 70;
hight = 25;

//torpint
//2 base parts
//1 coupler
//1 wall mount

//base();
print();

module print()
{
	rotate([90,0,0])
		intersection()
		{
			base();
			cube([length, width/2, hight]);
		}
	translate([0,2,width])
		rotate([270,0,0])
			intersection()
			{
				base();
				translate([0,width/2,0])
					cube([length, width/2, hight]);
			}
			
	translate([0,width+10,0])
		binder();
	
	translate([15,width*2-10,5])
			rotate([180,0,0])
		wallMount(0);
			
}

module base()
{
	difference()
	{
		translate([0,0,0.001])
			basicShape(hight);
		batteryNegetive();
		translate([19,26-17.5,11])
			connectorNegetive();
		translate([length/2,width/2,hight+0.01])
			mirror([0,0,1])
				minkowski()
				{
					binder();
					sphere(0.5, $fn = 5);
				}
		translate([0,0,hight+0.01])
			wallMount(0.5);
		translate([60,40,15])
			rotate([90,0,0])
				cylinder(width, 4,4);
		translate([60,3.5,16])
			rotate([90,0,0])
				cylinder(10, 8,8);
	}
}

module basicShape(hightB)
{
	
	linear_extrude(hightB)
	hull()
	{
		smallR = 4;
		bigR = 12.5;
		translate([0+smallR,0+smallR])
			circle(smallR);
		translate([0+smallR-1,width/2])
			circle(smallR);
		translate([0+smallR,width-smallR])
			circle(smallR);
		
		translate([length-bigR,0+bigR])
			circle(bigR);
		translate([length-smallR+1,width/2])
			circle(smallR);
		translate([length-bigR,width-bigR])
			circle(bigR);
		
	}
}

module batteryNegetive()
{
	//angled front part
	translate([0,width+1,0])
		rotate([90,0,0])
			linear_extrude(width+2)
				hull()
				{
					hightA = 11;
					roundR = 3;
					translate([-5,0])
						square([0.1,0.1], true);
					translate([-5,hightA])
						square([0.1,0.1], true);
					translate([13,0])
						square([0.1,0.1], true);
					translate([roundR,hightA-roundR])
						circle(roundR);
				}
	//sliding rail
	translate([0,4,0])
		rotate([90,0,90])
		{
			widthT = 44;
			depthRail = 3.5;
			hightT = 11;
			hightRail = 6;
			
			linear_extrude(17)
			{
				square([widthT, hightT]);
			}
			linear_extrude(55)
			{
				translate([0,hightRail])
					square([widthT,hightT-hightRail]);
			}
			linear_extrude(60)
			{
				translate([depthRail,0])
					square([widthT-depthRail*2, hightT]);
			}
		}
	//round end of rail
	linear_extrude(11)
		hull()
		{
			Radius = 7;
			translate([60, 7.5+Radius])
				circle(Radius);
			translate([60, width-7.5-Radius])
				circle(Radius);
		}
	//ramp hole
	translate([6,width/4*3,11])
		rotate([90,0,0])
			linear_extrude(width/2)
				hull()
				{
					hightA = 4;
					lengthA = 9;
					translate([0,0])
						square([0.2,0.2], true);
					translate([0,hightA])
						square([0.2,0.2], true);
					translate([lengthA,0])
						square([0.2,0.2], true);
				}
}

module connectorNegetive()
{
	widthCTop = 35;
	legnthCTop = 34;
	hightCTop = 2;
	
	widthCSmall = 31;
	lengthCSmall = 27;
	hightCSmall = 2;
	
	widthCBase = widthCTop;
	lengthCBase = 45;
	hightCBase = 3;
	
	translate([0,0,hightCBase+hightCSmall])
		cube([legnthCTop,widthCTop,hightCTop]);
	
	translate([3.5,2,hightCBase])
		cube([lengthCSmall,widthCSmall,hightCSmall]);
	
	cube([lengthCBase,widthCBase,hightCBase]);
}

module binder()
{
	size = width/5;
	
	linear_extrude(2)
		square([size,size*4],true);
	linear_extrude(5.5)
	{
		translate([0,size*1.5])
			square(size,true);
		translate([0,size*-1.5])
			square(size,true);
	}
}

module wallMount(negetive)
{
	widthM = width-10;
	hightM = 5;
	difference()
	{
		basicShape(hightM);
		translate([5,width/2,-1])
		{
			cylinder(hightM+2, 2, 2);		
			cylinder(hightM, 5, 2);
		}
		translate([length-5,width/2,-1])
		{
			cylinder(hightM+2, 2, 2);
			cylinder(hightM, 5, 2);
		}
	}
	translate([17,width,-hightM+1])
		rotate([90,0,0])
			minkowski()
			{
				linear_extrude(widthM)
				{
					sizeZ = 8;
					sizeX = 10/2;
					hull()
					{
						translate([0,sizeZ])
						circle(1);
						translate([sizeX,0])
							circle(1);
						translate([-sizeX,0])
							circle(1);
					}
					translate([36,0,0])
						hull()
						{
							translate([0,sizeZ])
							circle(1);
							translate([sizeX,0])
								circle(1);
							translate([-sizeX,0])
								circle(1);
						}
				}
				sphere(negetive, $fn = 12);
			}
				
}