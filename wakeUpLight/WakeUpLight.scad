$fn = 360;

M3BoltD = 1.5;
transition = 65;

print();
//view();


module print()
{/*
	bottom();
	
	translate([220,0,0])
		translate([0,0,-54]) 
			top();
	*/
	translate([0,150,0])
		alarmButton();
	
	translate([220,150,0])
		optionButtons();
	
	translate([100,150,0])
		cableClamp();	
}

module view()
{
	color([0.6,0.6,0.6])
		bottom();
	color([1,1,1])
		top();
	
	translate([0,0,10])	
		rotate([0,180,0])
			alarmButton();
	
	translate([0,0,10])
		rotate([-75,0,0])
			translate([0,2,72])
			{
				translate([0,0,-12])
					rotate([0,0,180])	
						screen();
				translate([0,0,3])
					rotate([0,180,0])
						optionButtons();
			}
				
}

module top()
{
	coupler();
	basicShape(start=transition);
	
}

module coupler()
{
	hull()
	{
		basicShape(transition-1,transition);
		translate([0,0,-10])
			scale([0.9,0.9,1])
				basicShape(transition-1,transition);
	}
}

module bottom()
{
	difference()
	{
		roundingWidth = 90;
		union()
		{
			basicShape(end = transition-1);
			translate([0,58,0])
				linear_extrude(10)
					circle(roundingWidth/2);
		}
		coupler();
		translate([0,0,transition-16])
			heatsink(true);
		translate([0,0,5])
			psu(true);
		cylinder(4,30,30, true);
		//UI
		translate([0,0,10])
		rotate([-75,0,0])
			translate([0,2,72])
			{
				//front shape
				hull()
				{
					linear_extrude(1)
						intersection()
						{
							circle(roundingWidth/2);
							translate([0,0.75,0])
							square([roundingWidth,38.5],true);
						}
					translate([0,2,35])
					cube([roundingWidth,65,1], true);
				}
				//window
				translate([0,-40,-4])
					cube([80, 120, 3], true);
				//screen
				translate([0,0,-12])
					rotate([0,0,180])	
						screen(true);
				translate([0,-15,-25])
					cube([80,50,30], true);
				
				//acces for lower screen bolts
				translate([0,20,-20])
					cube([80,10,10], true);
				
				//optionButtonGroves
				translate([0,0,3])
				rotate([0,180,0])
				optionButtons(true);
				
				//bottons
				xPosButton = 38;
				yPosButton = 10;
				zPosButton = -2.5;
				translate([0,0,0])
				{
				translate([xPosButton,yPosButton,zPosButton])
					rotate([0,0,90])	
						button(10);
				translate([xPosButton,-yPosButton,zPosButton])
					rotate([0,0,90])	
						button(10);
				translate([-xPosButton,-yPosButton,zPosButton])
					rotate([0,0,90])	
						button(10);
				translate([-xPosButton,yPosButton,zPosButton])
					rotate([0,0,90])	
						button(10);
				}
				
			}
		//buttons
		translate([0,90,3])
			button();
			
		//grove for the buttonplacement
		translate([-30,76,3])
			cube([60,3,10]);
			
		//hollows for pcb and cables. 
		translate([0,0,5])
			linear_extrude(transition)
			{
				translate([0,-50,0])
					square([100,25],true);
				square([160,70],true);
			}
		translate([0,0,15])
			linear_extrude(transition)
			{
				translate([0,-60,0])
					square([60,20],true);
			}
		translate([-55,-35,20])
			rotate([0,90,320])
				cylinder(50,15,15,true);
		translate([55,-35,20])
			rotate([0,90,40])
				cylinder(50,15,15,true);
		translate([-55,45,20])
			rotate([0,90,30])
				cylinder(50,5,5,true);
		
			
		//rear end
			//flat
			translate([0,-85,0])
				cube([40,10,60],true);
			//power cable hole
			translate([0,-75,15])
				rotate([90,0,0])
					cylinder(30,3,3, true);
			//cable binder
			translate([0,-66.5,-0.1])
			{
				linear_extrude(40)
				{
					translate([10,0])
						circle(1.5);
					translate([-10,0])
						circle(1.5);
				}
				linear_extrude(5)
				{
					translate([10,0])
						circle(3);
					translate([-10,0])
						circle(3);
				}
			}
	}
	
}
module optionButtons(negetive = false)
{
	roundingWidth = 86;
	spacing = 60; 
	
	groveWidth = 10;
	groveThickness = 2;
	
	difference()
	{	
		intersection()
		{
			hull()
			{
				linear_extrude(1)
					intersection()
					{
						circle(roundingWidth/2);
						square([roundingWidth,40],true);
					}
				translate([0,2,35])
				cube([roundingWidth,65,1], true);
			}
			union()
			{
				translate([-spacing + 20,0])
				{
					cube([20,35,4], true);
					translate([3.5,0,3])
					{
						if(negetive)
							cube([groveWidth+1,groveThickness+1,6],true);
						else
							cube([groveWidth,groveThickness,6],true);
					}
				}
				translate([spacing - 20,0])
				{
					cube([20,35,4], true);
					translate([-3.5,0,3])
					{
						if(negetive)
							cube([groveWidth+1,groveThickness+1,6],true);
						else
							cube([groveWidth,groveThickness,6],true);
					}
						
				}
			}
		}					
		translate([0,2,1.5])
			cube([90, 2, 2], true);
		translate([0,-2,1.5])
			cube([90, 2, 2], true);
	}
}

module alarmButton()
{
	
	translate([-29,76.5,0])
		cube([58,2,7]);
	
	difference()
	{
		linear_extrude(2)
			intersection()
			{
				translate([0,58,0])
					circle(45);
				translate([0,100,0])
					square([80, 50], true);
			}
		translate([0,80,1.5])
			cube([80, 2, 2], true);
	}
}

module cableClamp()
{
	difference()
	{
		cube([30,5,5],true);
		translate([-10,0])
			cylinder(10,1.5,1.5,true);
		translate([10,0])
			cylinder(10,1.5,1.5,true);
	}
}

module basicShape(start = 0, end = 250)
{
	increment = 1;
	
	for(i = [start+increment: increment: end])
	{
		Xscale = -0.0008*pow(i-25,2) + 100;
		Yscale = -0.0005*pow(i-25,2) + 85;
		hull()
		{
			translate([0,0,i-increment])
				linear_extrude(0.01)
					scale([Xscale,Yscale])
						circle(1);
			
			translate([0,0,i])
				linear_extrude(0.01)
					scale([Xscale,Yscale])
						circle(1);
		}
	}	
}

module heatsink(negetive = false)
{
	if(negetive)
	{
		edgeClearance = 4;
		heatsink = [120+edgeClearance*2,100+edgeClearance*2,7+edgeClearance];
		
		translate([0,0,heatsink[2]/2])
			cube(heatsink, true);
		
		difference()
		{
			cornerSize = 13*2;
			cube(heatsink, true);
			
			translate([heatsink[0]/2, heatsink[1]/2])
				cube([cornerSize,cornerSize,20],true);
			translate([-heatsink[0]/2, heatsink[1]/2])
				cube([cornerSize,cornerSize,20],true);
			translate([heatsink[0]/2, -heatsink[1]/2])
				cube([cornerSize,cornerSize,20],true);
			translate([-heatsink[0]/2, -heatsink[1]/2])
				cube([cornerSize,cornerSize,20],true);
		}
		
		//screw holes
		translate([heatsink[0]/2-edgeClearance*2,0])
		{
			translate([0,heatsink[1]/2-edgeClearance*2])	
				cylinder(heatsink[2],M3BoltD,M3BoltD, true);
			translate([0,-heatsink[1]/2+edgeClearance*2])	
				cylinder(heatsink[2],M3BoltD,M3BoltD, true);
		}
		translate([-heatsink[0]/2+edgeClearance*2,0])
		{
			translate([0,heatsink[1]/2-edgeClearance*2])	
				cylinder(heatsink[2],M3BoltD,M3BoltD, true);
			translate([0,-heatsink[1]/2+edgeClearance*2])	
				cylinder(heatsink[2],M3BoltD,M3BoltD, true);
		}
	}
	else
	{
		heatsink = [120,100,7];
		
		translate([0,0,heatsink[2]/2])
			cube(heatsink, true);
	}
}

module screen(negitive = false)
{
	if(negitive)
	{
		pcb = [73, 44, 10];
		screen = [60, 33, 40];
		
		xBoltDist = 68;
		yBoltDist = 39;
		
		translate([0, 0, screen[2]/2])
			cube(screen, true);
		
		translate([-0.4,-2.9,0])
		{
			difference()
			{
				translate([0,0,0])
					cube(pcb, true);
				translate([pcb[0]/2, pcb[1]/2, pcb[2]/2])
					cube([9, 20,6], true);
				translate([-pcb[0]/2, pcb[1]/2, pcb[2]/2])
					cube([9, 20,6], true);
				translate([pcb[0]/2, -pcb[1]/2, pcb[2]/2])
					cube([9, 20,6], true);
				translate([-pcb[0]/2, -pcb[1]/2, pcb[2]/2])
					cube([9, 20,6], true);
			}
			
			boltLength = 25;
			translate([xBoltDist/2, yBoltDist/2, -5])
				cylinder(boltLength,M3BoltD,M3BoltD, true);
			translate([-xBoltDist/2, yBoltDist/2, -5])
				cylinder(boltLength,M3BoltD,M3BoltD, true);
			translate([xBoltDist/2, -yBoltDist/2, 0])
				cylinder(boltLength,M3BoltD,M3BoltD, true);
			translate([-xBoltDist/2, -yBoltDist/2, 0])
				cylinder(boltLength,M3BoltD,M3BoltD, true);
		}
	}
	else
	{
		pcb = [73, 44, 5];
		screen = [60, 33, 3];
		
		translate([0, 0, screen[2]/2+2.5])
			color([0,0,0])
				cube(screen, true);
		
		translate([-0.4,-2.9,2.5])
			cube(pcb, true);
	}
}

module button(rearCutout = 4)
{
	linear_extrude(3)
		square([7,7], true);
	translate([0,0,-rearCutout])
		linear_extrude(rearCutout+0.01)
			difference()
			{
				square([10,10], true);
				square([5,11], true);
			}
}

module psu(negitive = false)
{
	if(negitive)
	{
		edgeClearance = 4;
		psu = [110,80,37];
		psuEdge = [psu[0]+edgeClearance*2,psu[1]+edgeClearance*2,psu[2]+edgeClearance+0.1];
		translate([0,0,psuEdge[2]/2])
		cube(psuEdge, true);
		
		//cable clearance
		translate([psu[0]/2,20-55,10])
			cube([20,55,15]);
		
		//screw holes
			//label side
		/*
		translate([-psu[0]/2+12.2,psu[1]/2-0.1,14.5])
			rotate([-90,0,0])
				cylinder(15,M3BoltD,M3BoltD);
		translate([psu[0]/2-10,psu[1]/2-0.1,21])
			rotate([-90,0,0])
				cylinder(15,M3BoltD,M3BoltD);*/
		
			//bottom
		translate([-psu[0]/2+43,psu[1]/2-24.5])
			cylinder(20,M3BoltD,M3BoltD, true);
		translate([-psu[0]/2+43,psu[1]/2-52.5])
			cylinder(20,M3BoltD,M3BoltD, true);
		translate([psu[0]/2-31.5,psu[1]/2-52.5])
			cylinder(20,M3BoltD,M3BoltD, true);
	}
	else
	{
		psu = [110,80,37];
		translate([0,0,psu[2]/2])
			cube(psu, true);
	}
}