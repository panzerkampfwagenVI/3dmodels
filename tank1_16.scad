
   
tank();

fenderwidth = 4.4;    
hullwidth = 14;
hullhight = 8.5;
groundhight = 2.7;
turretX = 27;

skirtwith = 0.5;
trackthickness = 0.3;

module ground()
{   
    translate([-500,-500,-0.11])
    {
        color([0,1,1,0.7])
        {
            cube([1000,1000,0.1]);
        }   
    }  
}

module tank()
{
translate([0,0, groundhight]){hul();}
translate([0,0,0])
{
	color([1,0,0])suspention();
	color([0,0,0])tracks();
}
translate([0,hullwidth,0])
{
	color([1,0,0])mirror([0,1,0])suspention();
	color([0,0,0])mirror([0,1,0])tracks();
}
translate([turretX,hullwidth/2,groundhight+hullhight]){color([1,0,0])
	{
		//rotate([0,0,180])
		turret();
		}}
}

module hul()
{   
	translate([0,hullwidth,0])color([0,0,1]){romp();}
	translate([0,0,0]){fender();} //fenders	
	translate([0,hullwidth+fenderwidth,0]){fender();}	

translate([0,-(fenderwidth),0]){sideskirt();} //sideskirts
translate([0,(fenderwidth+hullwidth+skirtwith),0]){sideskirt();}  

//translate([15,(hullwidth/2),6.5]){color([0,0,0]){turretring();}}
}

module romp()
{  		
	rotate([90,0,0])
	{
		points = [[7.5,0], [43.5,0], [47.5,4.5], [46,7], [37.5,hullhight], [0,hullhight], [0,6], [2.5,1]];
		
		linear_extrude(hullwidth, true){
			polygon(points);
		}
	}
}
module fender()
{
	rotate([90,0,0])
	{
		points = [[0,7],[46,7],[37.5,hullhight],[0,hullhight]];
		linear_extrude(fenderwidth)polygon(points);
	}
}

module sideskirt()
{
	rotate([90,0,0])
	{
		points = [[0,4],[46,4],[46,7],[37.5,hullhight],[0,hullhight]];
		linear_extrude(skirtwith)polygon(points);
	}
}

module turretring()//not updated yet
{
    $fn = 360;
    difference()
    {
        cylinder(0.4,6,6, true);
        cylinder(1,5.9,5.9, true);
    }
}

module suspention()
{
	//hight underside fender = 7 + 2.7 = 9.7. (underside fender+groundclear)
    $fn = 360;
	centershift = -0.7;
    translate([45,centershift,6.5])
    {
        rotate([90,0,0])
        {
            frontWheel();
        }   
    }
	
	for(i=[0:1:4])
	{
		position = 10.75+(7.05*i);
		translate([position,centershift,3.4])
		{
			rotate([90,0,0])
			{
				roadWheel();
			}
			translate([0,-1.90,0])
			{
				rotate([0,-20,0])
				{
					suspencionArm();
				}
			}
		}
	}
    translate([4,centershift,6.2])
    {
        rotate([90,0,0])
        {
            driveWheel();
        }   
    } 
}

module tracks()
{
	thickness = 0.4;
	drive = 3;
	road = 3;
	front = 2.5;
	difference()
	{
		hull()
			trackWheelPoints(drive+thickness, road+thickness,front+thickness,0); 
		translate([0,1,0])
			hull()
				trackWheelPoints(drive,road,front,2); 
	}
}

module trackWheelPoints(drive, road, front, tolerance)
{
	$fn = 30;
	trackWidth = 4+tolerance;
	centershift = -0.2;
	translate([0,centershift,0])
	{
		translate([45,0,6.5])						//frontwheel
			rotate([90,0,0])
				cylinder(trackWidth,front,front);
		translate([10.75,0,3.4])					//roadwheels
		{
			rotate([90,0,0])
				cylinder(trackWidth,road,road);
			translate([7.05*4,0,0])
				rotate([90,0,0])
					cylinder(trackWidth,road,road);
		}
		translate([4,0,6.2])
			rotate([90,0,0])
				cylinder(trackWidth,drive,drive);
	}
}

module suspencionArm()
{
	rotate([270,0,0])//arm to wheel
	{
		cylinder(3.6,0.4,0.4); //1.9 in wiel 2.4 tot romp 
	}
	translate([0,3.2,0])
	{
		rotate([0,90,0])
		{
			cylinder(4,0.4,0.4);
		}
		translate([4,0,0])
		{
			rotate([90,0,0])
			{
				cylinder(0.4,1,1,true);//bearing hull
			}
		}
	}
}

module frontWheel()
{
    cylinder(3,2.5,2.5);
	rotate([0,180,0])
	{
		cylinder(8,0.4,0.4);
	}
} 

module roadWheel()
{
    cylinder(3,3,3);
}

module driveWheel()
{
    cylinder(3,3,3);
	rotate([0,180,0])
	{
		cylinder(8,0.4,0.4);
	}
}

module turret()
{   
	union()
	{
		turretSide();
		mirror([0,1,0])
			turretSide();
		//rotate([0,180,0])
			//turretSide();
		color(0,1,1)
		{
			translate([9,0,2.25]){ rotate([0,90,0]){ gun();}}
		}
	}		
}

module turretSide()
{
	readEnd = -15;
	frontBottom = 10.5;
    turretHight = 5.0;
    points =    [
			[readEnd,0,0],[readEnd,6,0],[0,10.5,0],[6,9,0],[frontBottom,2.25,0],[frontBottom,0,0],	//bottom
			[readEnd,0,turretHight-0.2],[readEnd,4.2,turretHight-0.2],[0,8.25,turretHight+0.5],  	//top
			[4.5,7.2,turretHight],[6,2.25,turretHight],[6,0,turretHight],[0,0,turretHight+0.5],
			[11.25,6,2.25],[12.75,2.25,2.25],[12.75,0,2.25]											//front
			];
    
    faces =     [[0,1,2,3,4,5],             	//bottom
                [6,7,8,9,10,11,12],				//top
				[0,1,7,6],[1,2,8,7],[2,3,9,8],	//rear/side
				[9,3,13],						//front side
				[3,4,14,13],[4,5,15,14],		//front bottom
				[9,10,14,13],[10,11,15,14],		//front top
				[0,6,12,11,15,5]
				];  
	polyhedron(points,faces);
}

module gun()
{
    $fn = 30;
    difference()
    {
        cylinder(30,1.2,1.05);
        cylinder(32,0.75,0.75);
    }
}





















