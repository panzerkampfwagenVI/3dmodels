$fn = 120;

widthIndicatorBox = 65*2;
WidthConnectorBox = 40;

hightBoxAll = 60;
hightStepTwo = 40;
hightStepOne = 10;

depthBox = 70;
depthStepTwo = 30;
depthStepOne = 50;

wallThickness = 1;
wallThicknessConnectorBox = 2;

backplateSupportPinR = 3;

printBoxAll();

module printBoxAll()
{
	translate([0,0,depthBox])
		rotate([0,90,0])
			indicatorBox();
	
	translate([hightBoxAll+wallThickness,0,wallThickness])
		rotate([0,90,0])
			indicatorBoxBackplate();
	
	translate([-depthBox-wallThickness,0,WidthConnectorBox])
		rotate([-90,0,0])
			connectorBox();
}

module indicatorBox()//hole for connection
{
	difference()
	{
		cube([depthBox, widthIndicatorBox, hightBoxAll]);
		//hollow box
		translate([-wallThickness,wallThickness,wallThickness])
			cube([depthBox, widthIndicatorBox-wallThickness*2, hightBoxAll-wallThickness*2]);
		//indicator mounts
		translate([1,32.5,2])	
			indicatorMount();
		translate([1,widthIndicatorBox-32.5,2])	
			indicatorMount();
		//hole for wires to connectorbox, lined up with the connectorbox.
		translate([depthStepTwo/2+wallThicknessConnectorBox,widthIndicatorBox,depthStepTwo/2+wallThicknessConnectorBox])
			rotate([90,0,0])
				cylinder(wallThickness*2+1,depthStepTwo/2,depthStepTwo/2,true);	
	}
	
	//supportfeet for backplate
	intersection()
	{
		//original cube size
		cube([depthBox, widthIndicatorBox, hightBoxAll]);
		//mountingbrackets
		union()
		{
			translate([wallThickness,wallThickness,wallThickness])
				rotate([0,90,0])
					cylinder(10,backplateSupportPinR,0);
			translate([wallThickness,widthIndicatorBox-wallThickness,hightBoxAll-wallThickness])
				rotate([0,90,0])
					cylinder(10,backplateSupportPinR,0);
			translate([wallThickness,wallThickness,hightBoxAll-wallThickness])
				rotate([0,90,0])
					cylinder(10,backplateSupportPinR,0);
			translate([wallThickness,widthIndicatorBox-wallThickness,wallThickness])
				rotate([0,90,0])
					cylinder(10,backplateSupportPinR,0);
			translate([wallThickness,widthIndicatorBox/2,hightBoxAll-wallThickness])
				rotate([0,90,0])
					cylinder(10,backplateSupportPinR,0);
			translate([wallThickness,widthIndicatorBox/2,wallThickness])
				rotate([0,90,0])
					cylinder(10,backplateSupportPinR,0);
		}
	}
}

module indicatorBoxBackplate()
{
	cube([wallThickness, widthIndicatorBox-wallThickness*2-1, hightBoxAll-wallThickness*2-1]);
}

module connectorBox()
{
	difference()
	{
		union()
		{
			cube([depthBox,WidthConnectorBox,hightStepOne]);
			cube([depthStepOne,WidthConnectorBox,hightStepTwo]);
			cube([depthStepTwo,WidthConnectorBox,hightBoxAll]);
		}
		//hollow inside
		translate([wallThicknessConnectorBox,wallThicknessConnectorBox,wallThicknessConnectorBox])		
		cube([depthBox-wallThicknessConnectorBox*2,WidthConnectorBox-wallThicknessConnectorBox*2,hightStepOne-wallThicknessConnectorBox*2]);
		translate([wallThicknessConnectorBox,-wallThicknessConnectorBox,wallThicknessConnectorBox])
		{
			cube([depthStepOne-wallThicknessConnectorBox*2,WidthConnectorBox,hightStepTwo-wallThicknessConnectorBox*2]);
			cube([depthStepTwo-wallThicknessConnectorBox*2,WidthConnectorBox,hightBoxAll-wallThicknessConnectorBox*2]);
		}
		
		//holes for wireconnectors
		translate([depthBox-WidthConnectorBox/4,WidthConnectorBox/4,-1])	
			cylinder(hightStepOne*2,7.5/2,7.5/2);
		translate([depthBox-WidthConnectorBox/4,WidthConnectorBox/4*3,-1])
			cylinder(hightStepOne*2,7.5/2,7.5/2);
		
		//hole for potmeter
		translate([depthStepTwo+(depthStepOne-depthStepTwo)/2,WidthConnectorBox/4*3,hightStepTwo-wallThicknessConnectorBox])
		{
			cube([7.5,7,5],true);
			cylinder(20,3.2,3);
		}
		
		//hole for output switch
		translate([depthStepTwo,WidthConnectorBox/4,hightBoxAll-(hightBoxAll-hightStepTwo)/2])
		{
			rotate([0,90,0])
				cylinder(10,3,3,true);
		}
		
		//hole for input switch
		translate([wallThicknessConnectorBox+0.5,WidthConnectorBox-wallThicknessConnectorBox-1,wallThicknessConnectorBoxConnectorBox+2])
		{
			cube([23,10,30]);
		}
	}
}

module indicatorMount()
{
	rotate([0,90,0])
	{
		linear_extrude(depthBox)
		{
			translate([-25-2.6,0,0])
				circle(25);
			translate([-13.75,25.9,0])
				circle(2);
			translate([-13.75,-25.9,0])
				circle(2);
			
		}
	}
}