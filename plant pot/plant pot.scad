//----wide pot---
/*
wallThicknessPot = 5;
hightPot = 50;
radiusPot = 40;
widthPot = 200;
hightFeet = 3;
*/

//----round pot

wallThicknessPot = 5;
hightPot = 130;
radiusPot = 75;
widthPot = 0;
hightFeet = 3;


plantPot(wallThicknessPot,hightPot,radiusPot,widthPot,hightFeet);



$fn = 180;


//wallThickness: 	the thickness of the outside walls
//hight: 			the total hight of the plant pot (including feet)
//outsideRadius:	the outside radius of the plant pot
//width: 			if width>outsideRadius, this will be regarded as the width of the pot
//feethight:		the hight of the feet
module plantPot(wallThickness, hight, outsideRadius, width, feethight)
{
	if(width!=undef && width>outsideRadius*2)
	{
		width = width - outsideRadius*2;
		difference()
		{
			hull()
			{
				shapePot(hight,outsideRadius);
				translate([width,0,0])
					shapePot(hight,outsideRadius);
			}
			hull()
			{
				translate([0,0,wallThickness+feethight+0.1])
				{
					shapePot(hight-wallThickness-feethight,outsideRadius-wallThickness);
					translate([width,0,0])
						shapePot(hight-wallThickness-feethight,outsideRadius-wallThickness);
				}
			}
			linear_extrude(feethight)
			{
				hull()
				{
					square([outsideRadius*2,outsideRadius*2],true);
					translate([width,0,0])
						square([outsideRadius*2,outsideRadius*2],true);
				}
			}
			rotate([0,0,90])
				feet(outsideRadius,wallThickness+0.5,feethight);
			rotate([0,0,180])
				feet(outsideRadius,wallThickness+0.5,feethight);
			translate([width,0,0])
			{
				rotate([0,0,0])
					feet(outsideRadius,wallThickness+0.5,feethight);
				rotate([0,0,-90])
					feet(outsideRadius,wallThickness+0.5,feethight);
			}
		}
	}
	else
	{
		difference()
		{
			shapePot(hight,outsideRadius);
			translate([0,0,wallThickness+feethight+0.1])
				shapePot(hight-wallThickness,outsideRadius-wallThickness-feethight);
			linear_extrude(feethight)
					circle(outsideRadius+0.1);
			for(i=[0:1:4])
			{
				rotate([0,0,i*90])
					feet(outsideRadius,wallThickness+0.5,feethight);
			}
		}
	}
	for(i=[-2:1:1])
	{
		translate([outsideRadius/2*i,outsideRadius,feethight])
			feet(outsideRadius,wallThickness,feethight-1);
	}
}

module feet(radius, wallThickness, hight)
{
	linear_extrude(hight)
		intersection()
		{
			difference()
			{
				circle(radius/4*3);
				circle(radius/4*3-wallThickness);
			}
			translate([wallThickness,wallThickness,0])
			square([radius,radius]);
		}
	for(i=[-1:1:1])
	{
		rotate([0,0,45+i*25])
			translate([radius/4*3-wallThickness/2,0,0])
				linear_extrude(hight*2)
					circle(wallThickness/2);
	}
}

module shapePot(hight, radius)
{
	edge = 20;
	hull()
	{
		translate([0,0,edge])
		minkowski()
		{
			cylinder(1,radius-edge,radius-edge);
			sphere(edge);
		}
		translate([0,0,hight])
		cylinder(1,radius,radius);
	}
}
