$fn = 120;

//plank = 7
//bout = 16
//moer = 4.5
//plastic = 4

wall = 10;
hight = 70;
length = 200;
rounding = 6;
boltPos = [40,160];

mount();

module mount()
{
	difference()
	{
		union()
		{
			hookOffset = 13;
			
			linear_extrude(wall)
				square([length, wall]);
			translate([-hookOffset,0,wall/2])
				rotate([0,0,0])
					hook();
			translate([135,wall-1,wall/2])
				rotate([0,0,90])
					ring();
			translate([length+hookOffset,0,wall/2])
				mirror([1,0,0])
					hook();
		}
		for(i = boltPos)
			translate([i,wall+0.1,wall/2])
				rotate([90,0,0])
					nutHole();
	}
}
module hook()
{
	ID = 10;
	OD = ID+4;
	quaterOffset = 6;
	
	translate([0,quaterOffset-0.01,0])
		intersection()
		{
			rotate_extrude()
			{
				translate([ID+rounding,0,0])
						circle(rounding);
			}
			translate([5,0,0])
				cylinder(wall, OD, OD,true);
			translate([0,-quaterOffset,-wall/2])
				cube([20,20,wall]);
		}
}

module ring()
{
	ID = 6;
	ringD = ID+rounding;
	OD = ID+rounding*2;
	legLength = 5;
	translate([legLength,0,0])
	{
		intersection()
		{
			rotate_extrude()
				{
					translate([ringD,0,0])
							circle(rounding);
				}
			translate([OD/2,0,0])
				cube([OD,OD*2,wall],true);
		}
		for(i = [-ringD,ringD])
		{
			translate([-0.01,i,0])
				rotate([0,-90,0])
					linear_extrude(5.02)
						intersection()
						{
							circle(rounding);
							square([wall, rounding*2], true);
						}
		}
	}
}

module nutHole()
{
	cylinder(5,4.6,4.6, $fn = 6);
	cylinder(wall*2,2.5,2.5);
}










