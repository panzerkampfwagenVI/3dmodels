$fn = 120;

//plank = 7
//bout = 16
//moer = 4.5
//plastic = 4

wall = 9;
hight = 70;
length = 200;

mount();
translate([10,10,0])
arm();

module mount()
{
	difference()
	{
		union()
		{
			linear_extrude(wall)
			difference()
			{
				shape();
				translate([0,wall])
					square([length, hight]);
				translate([wall/2,wall*1.5])
					circle(wall/2+1);
			}
			inc = wall/5;
			for(i=[0:1:1])
			{
				translate([wall,wall*2,(i+0.5)*(inc*2+0.1)])
					linear_extrude((inc)-0.2)
						rotate([0,0,180])
							hinge();
			}
		}
		translate([length-wall/2,8.2,wall/2])
			rotate([90,0,0])
				cylinder(9,3,3);
		translate([length/5*4,9.1,wall/2])
			rotate([90,0,0])
				nutHole();
		translate([length/5,9.1,wall/2])
			rotate([90,0,0])
				nutHole();
	}
}

module arm()
{
	difference()
	{
		union()
		{
			linear_extrude(wall)
			difference()
			{
				shape();
				square([length, wall]);
				square([wall*3,wall*4],true);
				translate([wall/2,wall*1.5])
					circle(wall/2+1);	
			}
			inc = wall/5;
			echo(inc);
			for(i=[0:1:2])
			{
				translate([0,wall,(i)*(inc*2+0.1)])
					linear_extrude((inc)-0.2)
						hinge();
			}	
		}
		translate([length-wall/2,wall+8.8,wall/2])
			rotate([90,0,0])
				hull()
				{
					cylinder(8,3,3);
					translate([-wall,0,0])
						cylinder(8,3,3);
				}
	}
}

module hinge()
{
	difference()
	{	
		union()
		{
			translate([0,wall/2])
				square([wall, wall]);
			translate([wall/2,wall/2])
				circle(wall/2);
		}
		translate([wall/2,wall/2])
			circle(2.5/2);
	}	
}

module shape()
{
	rounding = 34;
	difference()
	{
		union()
		{
			square([length, rounding]);
			translate([rounding,rounding])
			offset(rounding)
				square([length-rounding*2,hight-rounding*2]);
		}
		translate([wall,wall])
		{
			square([length-wall*2, rounding]);
				rounding = rounding - wall;
			translate([rounding,rounding])
			{
				offset(rounding)
					square([length-(rounding+wall)*2,hight-(rounding+wall)*2]);
			}
		}
	}
}

module nutHole()
{
	cylinder(5,4.5,4.5, $fn = 6);
	cylinder(wall*2,2.5,2.5);
}










