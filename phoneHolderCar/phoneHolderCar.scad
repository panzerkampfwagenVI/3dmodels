$fn = 120;

phoneSize = [143, 73, 10];
tollerance = 0.5;
backingThickness = 5;

overhangThickness = 2;
overhangGripper = 4.5;
distanceFromCorner = 5;
				
offsetHinge = 15;


view();
//holderMount();
//wholeHolder();
module view()
{
	extended = true;
	rotation = (extended) ? (70) : (160);
	rotate([-rotation,0,0])
		translate([0,49.5,-2])	
			wholeHolder();
	
	holderMount();
}

module holderMount()
{
	rotate([90,0,0])
		rotate([0,90,0])
		{
			translate([0,0,phoneSize[0]/2-offsetHinge])
				hinge(1);	
			translate([0,0,offsetHinge-phoneSize[0]/2])
				hinge(1);
		}
		
	translate([0,19,0])
	{
		mountThickness = 6;
		transZ = 18.5 - mountThickness;	
		translate([0,0,transZ])
		{
			linear_extrude(mountThickness)
			{
				ribLength = phoneSize[0];
				ribWidth = 10;
				ribSpacing = 40;
				square([ribLength, ribWidth/2],true);
				
				translate([0,-ribSpacing])
					square([ribLength, ribWidth],true);
				translate([0,-ribSpacing*2])
					square([ribLength, ribWidth],true);
				
				spineLength = 80;
				spineWidth = 20;
				translate([-spineWidth/2,-spineLength])
					square([spineWidth,spineLength]);
			}
		}
		groveThickness = 2;
		additionalGroveThickness = 2;
		translate([0,0,transZ-additionalGroveThickness])
		{
			linear_extrude(groveThickness+additionalGroveThickness)
			{
				groveBarLength = 80;
				groveBarWidth = 10;
				translate([phoneSize[0]/2-groveBarWidth,-groveBarLength])
					square([groveBarWidth,groveBarLength]);
				translate([phoneSize[0]/2.8-groveBarWidth,-groveBarLength])
					square([groveBarWidth,groveBarLength]);
				translate([-phoneSize[0]/2,-groveBarLength])
					square([groveBarWidth,groveBarLength]);
				translate([-phoneSize[0]/2.8,-groveBarLength])
					square([groveBarWidth,groveBarLength]);
			}
		}
	}
}

module wholeHolder()
{
	translate([0.01,-0.01,0])
		sideGrip();
	mirror([1,0,0])	
		translate([0.01,-0.01,0])
			sideGrip();
	mirror([0,1,0])	
		translate([0.01,-0.01,0])
			sideGrip();
	mirror([1,0,0])	
	mirror([0,1,0])	
		translate([0.01,-0.01,0])
			sideGrip();
	
	translate([0,-phoneSize[1]/2-13,2])
		rotate([180,0,0])
			rotate([0,90,0])
			{		
				translate([0,0,phoneSize[0]/2-offsetHinge])
					hinge(0);	
				translate([0,0,offsetHinge-phoneSize[0]/2])
					hinge(0);
			}
}
//hinge(0);
//hinge(1);

//option 0 = inside of hinge (inside will be on mount outside on phone)
//option 1 = outside of hinge
module hinge(option)
{
	hingeRadius = 10;
	hingeWidthTotal = 20;
	ballRadius = 2.5;
	bearingR = 1.25;
	
	if(option == 0)
	{
		
		hingeWidth	= hingeWidthTotal/2 - 2; //1 mill of play
		
		difference()
		{
			difference()
			{
					hightMount = phoneSize[2] + backingThickness + overhangThickness;
				hull()
				{
					cylinder(hingeWidth,hingeRadius,hingeRadius, true);
					translate([hightMount/2-2,-hingeRadius-5,0])	
						cube([hightMount,8,hingeWidth],true);
				}
				translate([hightMount/2+1,-hingeRadius-6,0])	
					cube([hightMount*0.8,8,hingeWidth+1],true);
			}
			cylinder(hingeWidth+1,bearingR,bearingR, true);
			
			rotate([0,0,-20])	//open position
				translate([0,hingeRadius+ballRadius*0.5,0])	
					sphere(ballRadius);
			
			rotate([0,0,70])		//closed position
				translate([0,hingeRadius+ballRadius*0.5,0])	
					sphere(ballRadius);
		}
	}
	else if(option == 1)
	{
		hingeWidth	= hingeWidthTotal/4;
		holderOffset = 1;
		difference()
		{
			union()
			{
				hull()//top block
				{
					translate([0,0,hingeWidth])
						cylinder(hingeWidth,hingeRadius,hingeRadius);
					translate([0,hingeRadius,hingeWidth*1.5])
						cube([hingeRadius*2,holderOffset*2,hingeWidth], true);
				}
				hull()//bottom block
				{
					translate([0,0,-hingeWidth*2])
						cylinder(hingeWidth,hingeRadius,hingeRadius);
					translate([0,hingeRadius,-hingeWidth*1.5])
						cube([hingeRadius*2,holderOffset*2,hingeWidth], true);
				}
				
				blockThickness = ballRadius*3;
				YBlockTrans = hingeRadius+holderOffset+blockThickness/2;
				translate([0,YBlockTrans,0]) // combine block
					cube([hingeRadius*2,blockThickness,hingeWidthTotal],true);
			}
			//shaft hole
			cylinder(hingeWidthTotal+1,bearingR,bearingR, true);
			//ballpinhole
			rotate([-90,0,0])	
				cylinder(hingeRadius*2, ballRadius,ballRadius);
			//spring hole
			YHoleTrans = hingeRadius+holderOffset+3.5;
			springThickness = 10;
			translate([0,YHoleTrans,0]) // combine block
				cube([hingeRadius*2+1,ballRadius,springThickness],true);
		}
		//printPlates for improved holes
		translate([0,13.15,0])
		color([1,0,0])
			cube([hingeRadius,0.2,hingeRadius],true);
	}
}

module sideGrip()
{
	gripperLength = 20;
	gripperArmLength = phoneSize[0]/2-distanceFromCorner;
	gripperArmHight = phoneSize[1]/2-overhangGripper;
	
	gripperTranslate = [-gripperArmLength,gripperArmHight,0];
	
	translate(gripperTranslate)
	{
		translate([0,0,phoneSize[2]+backingThickness+overhangThickness])
			rotate([0,90,0])
				linear_extrude(gripperLength)
					edge2D();
		
		linear_extrude(backingThickness)
			difference()
			{
				translate([0,-gripperArmHight])
					square([gripperArmLength,gripperArmHight]);
				
				translate([gripperArmLength,0])
					scale([1/(phoneSize[1]/4)*(gripperArmLength-gripperLength),1])
						circle(phoneSize[1]/4);
				
				difference()
				{
					translate([0,-gripperArmHight])
						square([gripperArmLength,gripperArmHight]);
					
					translate([gripperArmLength,0])
						scale([1/(phoneSize[1]/2)*(gripperArmLength),1])
							circle(phoneSize[1]/2);
					
					difference()
					{
						translate([10,-40])
							square([30,30]);
						translate([6.0,-gripperArmHight])
							scale([1.4,2])	
								circle(7);
					}
				}
			}
	}
}

module edge2D()
{
	hardEdge = 6;
	softEdge = phoneSize[2] - hardEdge;
	overhangThickness = 2;
	difference()
	{
		square([ phoneSize[2]+backingThickness+overhangThickness, overhangGripper+overhangThickness]);
		translate([overhangThickness,0])
			square([hardEdge, overhangGripper]);
			
		translate([overhangThickness+hardEdge,0])
			scale([1,1/softEdge*overhangGripper])
				circle(softEdge);
			
		translate([phoneSize[2]+overhangThickness,0])
		{
			scale([1,1/backingThickness*(overhangGripper+overhangThickness)])
				difference()
				{
					square(backingThickness);
					circle(backingThickness);
				}
		}
	}
}