$fn = 60;

kamerY = 422;
kamerX = 284;

hoekY = 94;
hoekX = 75;


nuKamer();

translate([400,0,0])
kamer2();

translate([800,0,0])
kamer3();


module kamer3()
{
	vloer();
	
	translate([0,kamerY - 180,0])
		rotate([0,0,00])
			bed(dubble = true);
	
	translate([kamerX,30,0])
		rotate([0,0,90])
			buro();
	//translate([62,kamerY,0])
	//	rotate([0,0,180])
	//		printerTafel();
	
	translate([60,100,0])
		rotate([0,0,90])
			klerekast();
	translate([50,50,0])
		rotate([0,0,90])
			bijzetKast();
	
	raam();
}

module kamer2()
{
	vloer();
	
	translate([kamerX-25,50,0])
		rotate([0,0,90])
			bed(dubble = true);
	
	translate([225,kamerY,0])
		rotate([0,0,180])
			buro();
	translate([62,kamerY,0])
		rotate([0,0,180])
			printerTafel();
	
	translate([60,135,0])
		rotate([0,0,90])
			klerekast();
	translate([50,85,0])
		rotate([0,0,90])
			bijzetKast();
	
	raam();
}

module nuKamer()
{
	vloer();
	
	translate([kamerX,50,0])
		rotate([0,0,90])
			bed(dubble = false);
	
	translate([85,80,0])
		rotate([0,0,90])
			buro();
	translate([85,245,0])
		rotate([0,0,90])
			printerTafel();
	
	translate([20,kamerY-60,0])
		rotate([0,0,0])
			klerekast();
	translate([115,kamerY-50,0])
		rotate([0,0,0])
			bijzetKast();
	
	raam();
}

module vloer()
{
	color([0.5,0.5,0.5,1])
	{
		square([kamerX,kamerY]);
		translate([kamerX,kamerY-hoekY])
			square([hoekX,hoekY]);
	}
}

module raam()
{
	hoogte = 240;
	color([0.9,0.9,1,0.2])
		linear_extrude(hoogte)
			square([kamerX,0.1]);
}

module buro()
{
	breedte = 160;
	diepte = 85;
	hoogte = 74;
	
	cube([breedte, diepte,hoogte]);
}

module printerTafel()
{
	breedte = 60;
	diepte = 85;
	hoogte = 70;
	
	cube([breedte, diepte,hoogte]);
}

module klerekast()
{
	breedte = 95;
	diepte = 60;
	hoogte = 200;
	
	cube([breedte, diepte,hoogte]);
}

module bijzetKast()
{
	breedte = 50;
	diepte = 50;
	hoogte = 135;
	
	cube([breedte, diepte,hoogte]);
}

module bed(dubble)
{
	breedte = 200;
	diepte = dubble ? 140 : 95 ;
	hoogte = 50;
	
	cube([breedte, diepte,hoogte]);
}