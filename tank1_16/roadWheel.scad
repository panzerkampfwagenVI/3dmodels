include <hardware/bolt.scad>

$fn = 120;

wheelWidth = 15;
wheelGapWidth = 3;
wheelOR = 30;

mountWidth = 12;
mountOR = wheelOR - 10;
mountIR = 4.5;
mountBearingHoleIR = 6;
mountBearingHoleWidth = 4;

nBolts = 6;
rBolts = (mountOR+mountIR)/2;
boltR = 1.5;

printWheel();

module printWheel()
{
	shaftMount();
	
	translate([0,mountOR+wheelOR+5,0])
		insideWheel();
	
	translate([mountOR+wheelOR+5,0,0])
		outsideWheel();
	
	//translate([mountOR+wheelOR+5,mountOR+wheelOR+5,0])
		//shaftStop();
}

module outsideWheel()
{
	holeOR = wheelOR - 5;
	holeIR = holeOR - 3;
	holeDepth = 10;
	holeRound = 2;
	
	spokePosition = wheelWidth-holeDepth+holeRound;
	spokeHightHigh = 9;
	spokeHightLow = 1;
	spokeWidthWide = 5;
	spokeWidthLow = 0.01;
	spokeRound = 1;
	spokeAngle = 5;
	
	capHight = 4;
	capBallR = 6;
	
	difference()
	{
		
		wheel();
		translate([0,0,wheelWidth-holeDepth+holeRound*2])
		minkowski()//hole
		{
			cylinder(holeDepth-holeRound,holeIR-holeRound,holeOR-holeRound);
			sphere(holeRound);
		}
	}
	
	translate([0,0,spokePosition])//spokes and centerpin
	{
		difference()
		{
			union()
			{
				translate([0,0,-capBallR+capHight])//center cap
				{
					sphere(capBallR,true);
				}
				degreeRotation = 360/nBolts;
				for(i=[0:degreeRotation:360])
				{
					rotate([0,0,i])
					{
						translate([0,0,0])
						{
							minkowski()
							{	
								hull()
								{
									cube([spokeWidthLow,1,spokeHightLow],true);
									translate([0,holeOR,0])
										cube([spokeWidthWide,1,spokeHightHigh],true);
								}
								sphere(spokeRound);
							}
						}
					}
				}
			}
			clearance = 30;
			translate([0,0,-clearance])//cleaning underside spokes
			{
				cylinder(clearance,wheelWidth*2-1,wheelWidth*2-1);
			}
		}
	}
}

module insideWheel()
{
	holeOR = wheelOR - 5;
	holeIR = holeOR - 3;
	holeDepth = 8;
	holeRound = 2;
	
	boltHight = wheelWidth - holeDepth + 1;
	difference()
	{
		
		wheel();
		translate([0,0,wheelWidth-holeDepth+holeRound*2])
		minkowski()
		{
			cylinder(holeDepth-holeRound,holeIR-holeRound,holeOR-holeRound);
			sphere(holeRound);
		}	
		
		degreeRotation = 360/nBolts;
		for(i=[0:degreeRotation:360])
		{
			rotate([0,0,i])
				translate([rBolts,0,boltHight])
					bolt(); 
		}
	}
}

module shaftMount()//needs holes for bolts
{
	difference()
	{
		rotate_extrude()
		{
			difference()
			{
				square([mountOR,mountWidth]);
				
				square([mountBearingHoleIR,mountWidth]);							//shafthole
				
			}
		}
		bolts();
	}
}

module wheel()	//needs holes for bolts
{
	difference()
	{
		rotate_extrude()
		{
			difference()
			{
				square([wheelOR, wheelWidth]);
				
				square([mountIR, wheelWidth]);
				
				translate([0,-(mountWidth+wheelGapWidth)/2,0])
					#square([mountOR+0.5,mountWidth]);
				
			}
		}
		bolts();
	}
}

module bolts()
{
	lengthBolts = wheelWidth*2+wheelGapWidth;
	degreeRotation = 360/nBolts;
	for(i=[0:degreeRotation:360])
	{
		rotate([0,0,i])
			translate([rBolts,0,-1])
				linear_extrude(lengthBolts)
					circle(boltR,true);
	}
}

module shaftStop()
{
	stopHight = mountWidth - mountBearingHoleWidth*2;
	stopID = 4;
	stopOD = 4.4;
	
	difference()
	{
		cylinder(stopHight,stopOD,stopOD);
		cylinder(mountWidth,stopID,stopID,true);
	}
}