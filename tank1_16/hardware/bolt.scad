$fn = 30;

module bolt()
{
	difference()
	{
		union()
		{	
			cylinder(4,2.7,2.7);
			cylinder(2.4,3.18,3.18, $fn=6);
		}
		translate([0,0,-1])
		{
			cylinder(6,1.499,1.499);
		}
	}
}