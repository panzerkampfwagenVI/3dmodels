
module servoHole(rotation, tollerance = 0.2)
{
	$fn = 60;
	width = 13+tollerance;
	length = 23+tollerance;
	hight = 23+tollerance;
	
	mountHight = 16+tollerance;
	mountLength = 33+tollerance;
	mountThickness = 2.5+tollerance;
	
	screwSpacing = 28;
	screwHoleD = 1;

	gearHight = 27;
	smallGearOD = 3;
	bigGearOD = width/2;
	
	shaftHight = 32;
	shaftOD = 2.25;
	
	translate([length/2-bigGearOD,0,shaftHight])
		rotate([0,0,rotation])	
			servoHeadhead();
	
	linear_extrude(hight)
		square([length,width], true);
	
	translate([0,0,mountHight])
	{
		linear_extrude(mountThickness)
			square([mountLength, width], true);
		linear_extrude(shaftHight-mountHight)
		{
			translate([-screwSpacing/2,0])
				circle(screwHoleD*2);
			translate([screwSpacing/2,0])
				circle(screwHoleD*2);
		}
			
	}
	
	linear_extrude(gearHight)
	{
		translate([length/2-bigGearOD,0])	
			circle(bigGearOD);
		
		translate([-1,0])	
			circle(smallGearOD);
	}
	
	linear_extrude(shaftHight)
		translate([length/2-bigGearOD,0])
			circle(shaftOD);
	
	linear_extrude(hight)
	{
		translate([-screwSpacing/2,0])
			circle(screwHoleD);
		translate([screwSpacing/2,0])
			circle(screwHoleD);
	}
	
	linear_extrude(hight)
	{
		translate([length/2,0])
			square([4,4],true);
	}
}

module servoHeadhead()
{
	armMountOD = 3.5;
	armEndOD = 2;
	
	totalArmLength = 19.5;
	armMountHight = 3.5;
	armHight = 1.5;
	rotate([0,180,0])
	{
		linear_extrude(armHight)
			hull()
			{
				circle(armMountOD);
				
				translate([totalArmLength-armMountOD-armEndOD,0])
					circle(armEndOD);
			}
		linear_extrude(armMountHight)
			circle(armMountOD);
	}
}