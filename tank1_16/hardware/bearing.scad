$fn=30;

bearingIR = 4;
bearingOR = 7;
beartingWidth = 4;

module bearing()
{
	difference()
	{
		cylinder(beartingWidth, bearingOR, bearingOR, true);
		cylinder(beartingWidth+0.001, bearingIR, bearingIR,true);
	}	
}