	$fn = 60;

hullHight = 85;
hullWidth = 100;

lidSize = [90,130,3];
boxOutsideSize = [25,100,7];


//rearHalfDetailed();
rear();
//view();
//print();
module print()
{
	fanCover();
	
	translate([-10,0,hullHight])
		rotate([0,180,0])
			rear();
}

module view()
{
	rear();
	translate([3,-lidSize[1]/2,hullHight-lidSize[2]])
		color([1,0,0]) fanCover();
}

module rear()
{
	interfaceBoxPos = [67,-boxOutsideSize[1]/2,hullHight-boxOutsideSize[2]-lidSize[2]];
	ArmorThickness = 4.7;
	difference()
	{
		union()
		{
			rotate([90,0,0])
				rearHalfDetailed();
			mirror([0,1,0])
				rotate([90,0,0])
					rearHalfDetailed();
		}
		translate(interfaceBoxPos)
		{
			interfaceBox(true);
			translate([2,1,-3.5])				
			cube([boxOutsideSize[0]+10,boxOutsideSize[1]-2,3.5]);
		}			
		cube([hullWidth*2,hullWidth,ArmorThickness*2],true);
		translate([0,hullWidth/2,0])
			rotate([90,0,0])
				linear_extrude(hullWidth)
					hull()
					{
						translate([18,10,0])
							circle(ArmorThickness);	
						translate([75,00,0])
							circle(ArmorThickness);	
					}
				
	}
	translate(interfaceBoxPos)
	{
		interfaceBox();
		translate([1,boxOutsideSize[1]/2,-2])
			cube([2,30,15],true);
	}
}

module uiPlate()
{
	cube([boxOutsideSize[0],boxOutsideSize[1]-3,3]);
}		

module rearHalfDetailed()
{
	difference()
	{
		union()
		{
			readHalf();
			fanMountThickness = 5;
			fanMountWidth = 15;
			translate([fanDistanceFromRear,hullHight-fanHight-lidSize[2]-0.5-fanMountThickness,0])
				cube([fanSize+3,fanMountThickness,fanMountWidth]);
			
			translate([100,80,82.5])
				rotate([-90,0,0])
					fenderConnection();	
		}
		
		fanSize = 61;
		fanHight = 16;
		boltHoleDistanceFromEdge = 5;
		boldHoleD = 2;
		fanDistanceFromRear = 5;
		translate([fanDistanceFromRear,hullHight-fanHight-lidSize[2]-0.5,0])// fan cutouts
		{
			translate([0,0,-0.2])
				cube([fanSize,fanHight+0.6,fanSize+0.2]);
			translate([fanSize/2,0,fanSize/2])
				hull()
				{
					rotate([-90,0,0])		
						cylinder(1,fanSize/2,fanSize/2);
					translate([0,-fanHight,0])
						rotate([90,0,0])
						cylinder(0.1,fanSize/2-fanHight,fanSize/2-fanHight);
				}
			rotate([90,0,0])
			{
				translate([boltHoleDistanceFromEdge,boltHoleDistanceFromEdge,0])
					cylinder(20,boldHoleD,boldHoleD,true);
				translate([-boltHoleDistanceFromEdge+fanSize,boltHoleDistanceFromEdge,0])
					cylinder(20,boldHoleD,boldHoleD,true);
				translate([boltHoleDistanceFromEdge,-boltHoleDistanceFromEdge+fanSize,0])
					cylinder(20,boldHoleD,boldHoleD,true);
				translate([-boltHoleDistanceFromEdge+fanSize,-boltHoleDistanceFromEdge+fanSize,0])
					cylinder(20,boldHoleD,boldHoleD,true);
			}	
		}
		shaftbaringD = 10;
		shaftPos = [40,35];
		translate(shaftPos)	//cutout for gearbox .
		{
			cylinder(hullWidth/2, shaftbaringD,shaftbaringD);
			gearboxSize = [58.2,40,hullWidth/2];
			rotate([0,0,0])
			translate([-20,-20,-5])
				color([0,1,0]) cube(gearboxSize);
		}
		
		lidDistanceFromRear = fanDistanceFromRear - 2;
		translate([lidDistanceFromRear,hullHight-lidSize[2],-0.2]) //lid cutout
			cube([lidSize[0],lidSize[2]+0.1,lidSize[1]/2+0.2]);
		
		cutoutLength = 75;
		translate([hullWidth-cutoutLength,0,0])//pcb mount
		{
			translate([0,0,-1])
				cube([cutoutLength,30,31]);
			translate([0,7,0])
				cube([cutoutLength,3,40]);	
		}
	}
	
}

module readHalf()
{
	width = hullWidth/2;
	wallWidth = 2.5;
	spacerWidth = 8;
	fenderWidth = 50;
	translate([0,0,-0.1]) //middle section
	{
		difference()
		{
			wallSpacer(width+0.1);
			
			translate([5,0,-0.1])	//hollow inside
				linear_extrude(hullWidth/2-5+0.1)
					difference()
					{
						templateRear();
						square([100,13]);
					}	
		}
	}
	translate([0,0,width+wallWidth])
	{
		wallSpacer(spacerWidth);
	}
	sideFender(width+wallWidth+spacerWidth+wallWidth+fenderWidth);
}

module wallSpacer(extrude)
{
	shaftPos = [40,35];
	mountRotation = 0;
	linear_extrude(extrude)
		difference()
		{
			templateRear();
			translate(shaftPos)
				rotate([0,0,mountRotation])
					motorMountNegetive();
		}
}

module sideFender(extrude)
{
	fenderThickness = 5;
	roundoffR = 30;
	bumperHight = 20;
	linear_extrude(extrude)
	{
		difference()
		{
			templateRear();
				offset(roundoffR) offset(-roundoffR)
					square([200,hullHight-fenderThickness]);
			translate([0,hullHight-bumperHight-50])
				square([100, 50]);
		}
	}
}

module motorMountNegetive()
{
	boltHoleD = 2;
	boldPos = [[-11,-14], [29,-14], [29,14], [-11,14]];
	axelD = 4.1;
	bearingD = 6.1;
	
	for(i=[0:1:3])
		translate(boldPos[i])
			circle(boltHoleD);
	
	circle(bearingD);
	
	hull()
	{
		circle(axelD);
		translate([60,0,0])
			circle(axelD);
	}
}

module interfaceBox(negetive = false)
{
	//negetive = false;
	wallThickness = 3;
	negInside = [boxOutsideSize[0] - wallThickness*2, boxOutsideSize[1] - wallThickness*2, boxOutsideSize[2] + 10];
	if(negetive)
	{
		negOutside = [boxOutsideSize[0], boxOutsideSize[1], boxOutsideSize[2] - 2];
		cube(negOutside);
		translate([wallThickness,wallThickness,wallThickness])
			cube(negInside);
	}
	else
	{
		difference()
		{
			cube(boxOutsideSize);
			translate([wallThickness,wallThickness,-0.1])
				cube(negInside);
		}
	}
}

module fenderConnection()
{
	ID = 1.75;
	OD = 5;
	width = 9;
	difference()
	{	
		hull()
		{
			translate([-OD*2,0,-0.1])
				cube([width,width,0.1]);
			translate([OD,0,-OD])
				rotate([-90,0,0])
					cylinder(width,OD,OD);
		}
		translate([OD,0,-OD])
			rotate([-90,0,0])
				cylinder(width*2+1,ID,ID,true);
	}
}

module fanCover()
{
	difference()
	{
		cube(lidSize);
		
		groveLength = 22;
		groveWidth = 2;
		groveAngle = -50;
		
		groveStepX = groveWidth+3.5;
		YOffset = 18.5;
		grovePosY = [YOffset, lidSize[1]/2-YOffset,,lidSize[1]/2+YOffset,lidSize[1]-YOffset];
		startPosX = 10;
		startPosY = lidSize[1]/8+3;
		nGrovesX = 9;
		nGrovesY = 4;
		
		for(i=[0:1:nGrovesX-1])
		{
			posX = startPosX + groveStepX*i;
			for(j=[0:1:nGrovesY-1])
			{
				posY = grovePosY[j];
				translate([posX,posY])
				rotate([0,groveAngle,0])	
					hull()
					{
						translate([0,-groveLength/2])
							cylinder(30,groveWidth/2,groveWidth/2,true);
						translate([0,groveLength/2])
							cylinder(30,groveWidth/2,groveWidth/2,true);
					}
			}
		}
	}
	translate([0,0,lidSize[2]*3/4])
	{
		hull()
		{
			translate([10,lidSize[1]/2], $fn=15)
				sphere(lidSize[2]/2);
			translate([57,lidSize[1]/2], $fn=15)
				sphere(lidSize[2]/2);
		}
		hull()
		{
			translate([57,5], $fn=15)
				sphere(lidSize[2]/2);
			translate([57,lidSize[1]-5], $fn=15)
				sphere(lidSize[2]/2);
		}
	}
}

module templateRear()
{  	
	cutoffLength = 100;
	//		  start		cutoff								 rest of rear
	points = [[75,0], [cutoffLength,0], [cutoffLength,85], [0,85], [0,60], [18,10]];
	polygon(points);
}