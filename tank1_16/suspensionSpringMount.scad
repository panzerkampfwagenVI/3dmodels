$fn = 30;

mountThickness = 8;
mountWidth = 40;
mountHight = 50;
mountLowSideOffset = 3;

guildXPossition = 15;
guildChamphere = 7;
guildWidth = 12;
guildHight = mountHight-5;
guildCutoffTopX = 10;
guildRoundoff = 2;

boltOD = 1.5;

springMount();

module springMount()
{
	linear_extrude(mountThickness)
	{
		difference()
		{
			startReference = 0;
			offset(r = guildRoundoff)
			offset(r = -guildRoundoff) 
			polygon([[startReference,mountLowSideOffset], 								//bottom left
					[guildXPossition,mountLowSideOffset], [guildXPossition,guildHight],	//guild
						[guildXPossition+guildWidth,guildHight],
						[guildXPossition+guildWidth,guildHight/2],
						[guildXPossition+guildWidth+guildChamphere,startReference],
					[mountWidth,startReference],										//bottom right
					[mountWidth,guildHight/2],
					[mountWidth-guildCutoffTopX,mountHight], 							//top
						[startReference+guildCutoffTopX,mountHight]]);

			translate([8,10])
				circle(boltOD);
			translate([33.5,24])
				circle(boltOD);
		}
	}
}