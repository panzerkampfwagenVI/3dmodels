include <track.scad>

$fn = 120;


wheelWidth = 15;
wheelGapWidth = 3;
wheelOR = 28.95; //31.2 bij nTrack = 15

trackEngagementThickness = 2;

mountWidth = 15;
mountOR = wheelOR - 7;
mountIR = 4;
mountShaftFlat = 1;

nBolts = 6;
rBolts = (mountOR+mountIR)/2;
boltR = 1.5;

shaftR = 4;				//shaft diameter = 8 mm
shaftFlatR = 3;			//shaft has 1 flat spot 3mm from center


trackThickness = 4;

driveWheelAll();

//testPrint();

module testPrint()
{	
	trackCutout();
}

module driveWheelAll()
{
	translate([0,0,0])
		driveWheelMount();
	translate([wheelOR*2+5,0,0])
		driveWheelOutside();
	translate([0,wheelOR*2+5,0])
		driveWheelInside();
}

module driveWheelMount()
{
	difference()
	{
		rotate_extrude()
		{
			difference()
			{
				square([mountOR,mountWidth]);
				
				square([mountIR+0.3,mountWidth]);							//shafthole
				
			}
		}
		bolts();
	}
	translate([-mountIR,mountIR-mountShaftFlat+0.3,0])
		cube([mountIR*2,mountShaftFlat,mountWidth]);
}

module driveWheelOutside()
{
	roundOver = 2;
	holeDepth = 5-trackEngagementThickness;
	holeOR = 20-roundOver;
	holeIR = 15-roundOver;
	
	boltSink = 2;
	difference()
	{
		union()
		{
			wheel();
			translate([0,0,wheelWidth])
				trackCutout();
		}
		bolts();
		
		rotate_extrude()
		{
			intersection()
			{
				offset(r =roundOver)
					hull()
					{
						translate([0,wheelWidth+trackEngagementThickness,0])
							square([holeOR,1]);
						translate([0,wheelWidth-holeDepth,0])
							square([holeIR,1]);
					}
				square([holeOR+roundOver,wheelWidth+trackEngagementThickness+roundOver+1]);
			}
		}
		degreeRotation = 360/nBolts;
		for(i=[0:degreeRotation:360])
		{
			rotate([0,0,i])
				translate([rBolts,0,wheelWidth-holeDepth-roundOver-boltSink])
					cylinder(5,3.25,3.52);
		}
	}
}

module driveWheelInside()
{
	roundOver = 2;
	holeDepth = 5-trackEngagementThickness;
	holeOR = 20-roundOver;
	holeIR = 15-roundOver;
	
	boltSink = 2;
	difference()
	{
		union()
		{
			wheel();
			translate([0,0,wheelWidth])
				trackCutout();
		}
		bolts();
		
		rotate_extrude()
		{
			intersection()
			{
				offset(r =roundOver)
					hull()
					{
						translate([0,wheelWidth+trackEngagementThickness,0])
							square([holeOR,1]);
						translate([0,wheelWidth-holeDepth,0])
							square([holeIR,1]);
					}
				square([holeOR+roundOver,wheelWidth+trackEngagementThickness+roundOver+1]);
			}
		}
		degreeRotation = 360/nBolts;
		for(i=[0:degreeRotation:360])
		{
			rotate([0,0,i])
				translate([rBolts,0,wheelWidth-holeDepth-roundOver-boltSink])
					cylinder(5,3.25,3.52, $fn=6);
		}
	}
}

module wheel()
{
	rotate_extrude()
	{
		difference()
		{
			square([wheelOR-0.5, wheelWidth]);
			
			square([mountIR+0.3, wheelWidth]);
			
			translate([0,-(mountWidth+wheelGapWidth)/2,0])
				square([mountOR+0.5,mountWidth]);
			
		}
	}
	translate([-mountIR,mountIR-mountShaftFlat+0.3,4.5])
		cube([mountIR*2,mountShaftFlat,mountWidth-4.5]);		
}


module bolts()
{
	lengthBolts = wheelWidth*2+wheelGapWidth;
	degreeRotation = 360/nBolts;
	for(i=[0:degreeRotation:360])
	{
		linear_extrude(lengthBolts)
			rotate([0,0,i])
				translate([rBolts,0,-1])
						circle(boltR);
	}
}

//new sprockets
module trackCutout()
{
	trackR = wheelOR+trackThickness/2;
	step = 14;			//length of each track peace
	nTracks = 14;		//number of tracks per turn
	turn = 360/nTracks;	//degrees of rotation of wheel per track part
	inc = 1;			//the increment for int forloop
	
	linear_extrude(trackEngagementThickness)
	{
		intersection_for(i = [0:1:14])
		{
			rotate([0,0,turn*i])
				intersection_for(i = [0:inc:step])
				{
					rotation = i*turn/step;
					rotate([0,0,rotation])
					difference()
					{
						circle(wheelOR+trackThickness*2);
						
						translate([i,trackR,0])
							trackLinks();
					}
				}
		}
	}
}

module trackLinks()
{
	step = 14;			//length of each track peace
	for(i = [-3:1:2])
	{ 
		translate([step*i,0,0])
			hull()
			{
				circle(thickness/2+0.1, $fn = 30);
				
				translate([lengthCopler,0])
					circle(thickness/2+0.1, $fn = 30);
			}
	}
}


/*
old sprockets 
module trackCutoutOLD()
{
	nTracks = 14;
	turn = 360/nTracks;
	linear_extrude(trackEngagementThickness)
	{
		difference()
		{
			union()
			{
				circle(wheelOR+trackThickness/2);
				
				for(i=[0:turn:360])
					rotate([0,0,i])
						translate([0,wheelOR+trackThickness/2])
							circle(2.5);
			}
			
			projection()
				rotate([90,0,0])
					for(i=[0:turn:360])
						rotate([0,i,0])
							translate([-4.5,0,-(wheelOR+trackThickness/2)])
								rotate([0,turn/2,0])
									translate([-lengthCopler,0,0])
										hull()
											outerLink();
		}
	}
}


module emulateTrack()
{
	nTracks = 14;
	turn = 360/nTracks;
	for(i=[0:turn:360])
	{
		rotate([0,i,0])
		{
			translate([-4.5,0,-(wheelOR+trackThickness/2)])
			{
				turnedTrack(turn/2);
			}
		}
	}
}
*/

