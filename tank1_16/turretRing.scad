include <gears.scad>

$fn = 360;

turretRingR = 62;
ringThickness = 8;
ringWith = 3.5;
nBalls = 25;

nTeethRingGear = 100;

spacerThickness = 1;

turretRingPrint();

//whole thickniss = 
//racer = 1mm
//ball 	= 5mm
//racer = 2mm
//ball	= 5mm
//racer = 1mm

module turretRingPrint()
{
	translate([0,0,0])
		spacer();
	translate([(turretRingR+ringThickness)*2+5,0,0])
		hullSideBearing();
	translate([0,(turretRingR+ringThickness)*2+5,0])
		turretSideBearing();
	translate([(turretRingR+ringThickness)*2+5,(turretRingR+ringThickness)*2+5,0])
			for(i=[0:180:360])
				rotate([0,0,i])
					translate([52,0,0])
					{
						difference()
						{
						innerspacerShape(10);
						cylinder(25,1.5,1.5,true);
						}
					}
}

module hullSideBearing()
{
	union()
	{
		//bearing ring
		quaterTurretBearing();
		
		//ring gear
		translate([0,0,ringWith/2])
		Gear(z=nTeethRingGear,h=ringWith, D=turretRingR*2);  
		
		//mount to exteriour
		difference()
		{
			linear_extrude()
			{
				
			}
		}
	}
}

module turretSideBearing()
{
	union()
	{
		quaterTurretBearing();
		difference()
		{
			linear_extrude(ringWith)
			{
				difference()
				{
					circle(turretRingR);
					minkowski()
					{
						square([50,50],true);
						circle(20);
					}
				}
			}
			for(i=[0:90:360])
				rotate([0,0,i])
					translate([52,0,2])
					{
						innerspacerShape(5);
						cylinder(10,1.5,1.5,true);
					}
			
		}
	}
}

module quaterTurretBearing()
{
	ballS = 2.55;
	rotate_extrude()
	{
		difference()
		{
			translate([turretRingR,0,0])
				square([ringThickness,ringWith]); 
			translate([turretRingR+ringThickness/2,ringWith,0])
			{
				circle(ballS, true); 
			}
		}
	}
}

module spacer()
{
	_offset = 360/nBalls;
	linear_extrude(1)
	{	
		difference()
		{
			circle(turretRingR+ringThickness);
			circle(turretRingR);
			for(i=[0:_offset:360])
				rotate([0,0,i])
					translate([turretRingR+4,0,4])
						circle(2.55, $fn=30);
		}
	}
}

module screwmount()
{
	baseSize = 10;
	circleSize = 4;
	translate([-5,0,0])
	{
		linear_extrude(3)
		{	
			difference()
			{
				hull()
				{
					square(baseSize,true);
					translate([circleSize+baseSize,0,0])
					circle(circleSize);
				}	
				square(baseSize,true);
				translate([circleSize+baseSize,0,0])
				circle(1.5);
			}
		}
	}
}

module innerspacerShape(extrusion)
{
	
	linear_extrude(extrusion)
	{
		square([5,50],true);
		circle(5, $fn = 36);
	}
}