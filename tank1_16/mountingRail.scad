$fn = 60;

print();

module print()
{
	rotate([90,0,0])	
	side3d();
	
	
	hull()
	{
		translate([40,-35,0])
			cube(3);
		translate([3,-35,170])
			cube(3);
	}
	hull()
	{
		translate([-30,-35,0])
			cube(3);
		translate([-3,-35,100])
			cube(3);
	}
	hull()
	{
		translate([-50,-35,0])
			cube(3);
		translate([-3,-35,240])
			cube(3);
	}
}

module side3d()
{
	length = 270;
			difference()
			{
				width2d = 14;
				hight2d = 65;
				beamThickness2d = 3;
				union()
				{
	translate([0,length,0])
		rotate([90,0,0])
					linear_extrude(length)
						side2d(width2d, hight2d, beamThickness2d);
				}
				
				rotate([90,0,90])
				{
					extrude = 6.5;
					linear_extrude(extrude)
					{
						holeWidth = 40;
						holeHight = hight2d - 20;
						holeTopOffset = 52;
						holeR = 6;
							
						holeUp = 10;
						holeOffset = 70;
						for(i=[-1:1:3])
						translate([holeOffset*i+15,holeUp])
						{
							wallHole(holeR,holeWidth,holeHight,holeTopOffset);
							//translate([-10,0])		//only for verification used
									//wallScrewHoles();
						}
					}
				}
			}
}

module side2d(width2d, hight2d, beamThickness2d)
{
	difference()
	{
		square([width2d,hight2d]);
		translate([beamThickness2d,beamThickness2d])	
			square([width2d-beamThickness2d,hight2d-beamThickness2d]);
	}
	
	mountHight = 5;
	mountWidth = 5;
	mountballR = 2;
	translate([width2d-mountWidth/2,beamThickness2d])
		mountingRail2d(mountHight, mountWidth, mountballR);
	
	translate([beamThickness2d, hight2d/12*5])
		rotate([0,0,270])
			mountingRail2d(mountHight, mountWidth, mountballR);
	
	translate([beamThickness2d, hight2d-mountWidth/2])
		rotate([0,0,270])
			mountingRail2d(mountHight, mountWidth, mountballR);
	
}

module mountingRail2d(hight, width, ballR)
{
	translate([0,hight])
		circle(ballR);
	polygon([[-width/2,0],[width/2,0],[0,hight+ballR]]);
}

module wallScrewHoles()
{
	holeR = 1.5;
	axelMount = [0,8.5];
	springMount1 = [22.5,24];
	springMount2 = [48,38];
	
	translate(axelMount)
		circle(holeR);
	translate(springMount1)
		circle(holeR);
	translate(springMount2)
		circle(holeR);
}


module wallHole(R, width, hight, topOffset)
{
	hull()
	{
		translate([0+R,0+R])
			circle(R);
		translate([width-R,0+R])
			circle(R);
		translate([width+topOffset-R,hight-R])
			circle(R);
		translate([topOffset+R,hight-R])
			circle(R);
	}
}