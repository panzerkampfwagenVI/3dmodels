$fn = 360;

stageLength = 30;

barrelIR = 2.75;
barrelOR = 4;

spoolOR = 13;
spoolLength = 18;

endCapHoleLength = 10;
endCapHoleIR = 4.25;
endCapLength = 7;

printOptions();

module printOptions()
{
	basestage();	
	nStages = 4;
	for(i =[1:1:nStages])	
	{
		translate([0,i*stageLength,stageLength+endCapLength])
			rotate([0,180,180])
				spoolStage();	
	}	
}

module basestage()
{
	difference()
	{
		linear_extrude(stageLength+endCapHoleLength+0.5)
		{
			hull()
			{
				square(spoolOR*2,true);
				translate([10,0,0])
					circle(spoolOR);
			}	
		}	
		linear_extrude(stageLength)
		{
			circle(barrelIR);
		}
		translate([0,0,0.5])
		{
			linear_extrude(stageLength-1)
			{
				hull()
				{
					circle(barrelIR);
					translate([30,0,0])
						circle(barrelIR);
				}
			}
		}
		translate([0,0,stageLength+0.5])
		{
			linear_extrude(endCapHoleLength)
				circle(endCapHoleIR);
		}
	}
}

module spoolStage()
{
	difference()
	{
		union()
		{
			translate([0,0,-endCapHoleLength])
				spoolBarrel();
			translate([0,0,spoolLength])
			{
				difference()
				{
					spoolWasher(stageLength-spoolLength+endCapLength);
				}
			}
		}
		translate([0,0,spoolLength+1])
		{
			opto();
		}
		translate([-barrelOR-2,0,spoolLength])
		{
			rotate([60,0,0])
				cylinder(100,1.5,1.5,true);
		}
		translate([0,0,stageLength-endCapHoleLength+endCapLength])
		{
			linear_extrude(endCapHoleLength)
				circle(endCapHoleIR);
		}
	}			
}
module spoolBarrel()
{
	translate([0,0,-0.01])
	linear_extrude(stageLength+endCapHoleLength)
		difference()
		{
			circle(barrelOR);
			circle(barrelIR);
		}
}

module spoolWasher(z)
{
	linear_extrude(z)
		difference()
		{
			circle(spoolOR);
			circle(barrelIR+0.5);
		}
}

module opto()
{
	optoX = 10+10;
	optoY = 14;
	optoZ = 6.5;
	
	slotX = 8;
	slotY = 4.5;
	linear_extrude(optoZ)
		difference()
		{
			translate([-barrelOR,-optoY/2,0])
			{
				square([optoX,optoY]);
			}
			square([slotX,slotY],true);
		}
}
module endcap()
{}
