include <hardware/bolt.scad>

$fn = 60;

wheelOR = 25;
wheelTotalWidth = 30;
wheelGapWidth = 3;

axelMountIR = 4;
axelMountOR = 8;
axelMountWidth = 7;

wheelMountIR = axelMountOR+1;
wheelMountOR = wheelMountIR+6;
wheelMountWidth = axelMountWidth+2;

boltR = 1.5;
boltHeadR = 3;
nBolts = 6;
nutR = 3.52;
nutFn = 6;

bearingOR = 6;
bearingWidth = 4;



printWheel();

module printWheel()
{
	translate([wheelOR*1.5,0,0])
		axelMount();
	
	translate([wheelOR*1.5,wheelOR,0])
		wheelMount();
	
	translate([0,0,0])
		innerWheel();
	
	translate([0,wheelOR*2+2,0])
		outerWheel();
	
	//translate([mountOR+wheelOR+5,mountOR+wheelOR+5,0])
		//shaftStop();
}

module axelMount()
{
	difference()
	{
		//the ring
		linear_extrude(axelMountWidth)
		{
			difference()
			{
				circle(axelMountOR);
				circle(axelMountIR);
			}
		}
		//screw mount posstion
		translate([0,0,axelMountWidth/2])
		{
			rotate([0,90,0])
			{
				translate([0,0,(axelMountOR+axelMountIR)/2])
					cylinder(axelMountOR,boltHeadR,boltHeadR);
					cylinder(axelMountOR,boltR,boltR);
			}
		}
	}
}				

module wheelMount()
{
	linear_extrude(wheelMountWidth)
		{
			difference()
			{
				circle(wheelMountOR-0.5);
				circle(wheelMountIR);
				boltMount();
			}
		}
}

module outerWheel()
{
	wheelCapR = 8;
	wheelCapRound = 20;
	wheelCapHight = 2;
	
	boltHight = 7;
	
	difference()
	{
		union()
		{
			wheel();
			translate([0,0,wheelCapHight])
			{
				intersection()
				{
					translate([0,0,wheelCapRound/3-0.5])	
						linear_extrude(wheelCapRound)
						{
							circle(wheelCapR);
						}
					sphere(wheelCapRound/2);
				}
			}
		}
		degTurn = 360/nBolts;
		x = 4;
		for(i =[0:degTurn:360])
		{
			rotate([0,0,i])
			{
				translate([0,(wheelMountOR+wheelMountIR)/2,boltHight])
				{
					linear_extrude(x)
					{
						circle(boltHeadR);
					}
				}
			}
		}
	}
}

module innerWheel()
{	
	nutHight = 6;
	
	difference()
	{
		wheel();

		linear_extrude(wheelTotalWidth)
		{
			circle(axelMountIR+1);
		}
		
		degTurn = 360/nBolts;
		x = 4;
		for(i =[0:degTurn:360])
		{
			rotate([0,0,i])
			{
				translate([0,(wheelMountOR+wheelMountIR)/2,nutHight])
				{
					linear_extrude(x)
					{
						circle(nutR, $fn = nutFn);//make hex for nut
					}
				}
			}
		}
	}
}

module wheel()
{
	wheelIR1 = wheelOR - 4;
	wheelIR2 = wheelIR1 - 2;
	
	wheelCapRoundoverR = 3;
	wheelCapR = wheelMountOR+1;
	wheelCapHight = 10;
	
	translate([0,0,-wheelGapWidth/2])
		difference()
		{
			union()
			{
				difference()
				{
					linear_extrude(wheelTotalWidth/2)
					{
						circle(wheelOR);
					}
					cylinder(wheelTotalWidth/2, wheelIR2, wheelIR1);
				}
				
				minkowski()
				{
					linear_extrude(wheelCapHight-wheelCapRoundoverR)
					{
						circle(wheelCapR-wheelCapRoundoverR);
					}
					sphere(wheelCapRoundoverR);
				}
				rotate([0,0,90])
					spoke();
				
			}
			linear_extrude(wheelGapWidth/2)
			{
				circle(wheelOR);
			}
			linear_extrude(wheelMountWidth/2)
			{
				circle(wheelMountOR);
			}
			linear_extrude(wheelTotalWidth)
			{
				boltMount();
			}
			translate([0,0,wheelMountWidth/2])
				linear_extrude(bearingWidth)
				{
					circle(bearingOR);
				}
			
			translate([0,0,-25])
			cube([75,75,50],true);
		}
}

module boltMount()
{
	degTurn = 360/nBolts;
	for(i =[0:degTurn:360])
	{
		rotate([0,0,i])
			translate([0,(wheelMountOR+wheelMountIR)/2,0])
				circle(boltR);
	}
}

module spoke()
{
	smallSideWidth = 2.5;
	smallSideHight = wheelTotalWidth-15;
	bigSideWidth = 6	;
	bigSideHight = wheelTotalWidth-4;
	length = wheelOR-1;
	
	roundoff = 2;
	
	degTurn = 360/nBolts;
	for(i =[0:degTurn:360])
	{
		rotate([0,0,i])
		{
			minkowski()
			{	
				hull()
				{
					cube([smallSideWidth-roundoff,1,smallSideHight-roundoff],true);
					translate([0,length-roundoff,0])
						cube([bigSideWidth-roundoff,1,bigSideHight-roundoff],true);
				}
				sphere(roundoff);
			}
		}
	}
}
