include <hardware.scad>

    $fn = 60;
/*
track thickness is 
copler*3+pad*2 = 3*3 + 15*2 = 39 mm
*/

//allTrack();
//printTrack();

amount = 10;
thickness = 4.0;

lengthPad = 13;
widthPad = 15;

lengthCopler = thickness+1;
widthCopler = 3;

holeS = 1.1;


module allTrack()
{
	tracklength = 9.5;
	for(i=[0:1:amount-1])
	{
		translate([i*(lengthPad+1),0,0])
		{
			track();
		}
	}
    translate([0,70,0])
    {
        explodedView();
    }
}

module printTrack()
{	
	translate([0,thickness*6,0])
		printPad();
	translate([0,thickness*4.5,0])
		printPad();
	translate([0,thickness*3,0])
		printOuterLink();
	translate([0,thickness*1.5,0])
		printOuterLink();
	translate([0,thickness*0,0])
		printInnerLink();
}

module explodedView()
{
	
	translate([-3.5,-30,0.001])
	{
		trackPin(25);
	}	
	translate([0,-30,0.001])
	{
		trackPin(25);
	}	
	translate([-3.5,-15,0])
	{
		outerLink();
	}
	translate([0,-7,0])
	{
		pad();
	}
	translate([-3.5,0,0])
	{
		innerLink();
	}
	translate([0,7,0])
	{
		pad();
	}
	translate([-3.5,15,0])
	{
		outerLink();
	}
}

module track()			
{
	$fn = 60;
	
	translate([-lengthCopler,0,0])
	{
		//trackPin(40);
	}	
    translate([0,0,0])
    {
        //trackPin(40);
    }
	translate([-lengthCopler,(-widthPad-widthCopler-0.25),0])
	{
		outerLink();
	}
	translate([0,(-widthPad-widthCopler-0.25)/2,0])
	{
		pad();
	}
	translate([-lengthCopler,0,0])
	{
		innerLink();
	}
	translate([0,(widthPad+widthCopler+0.25)/2,0])
	{
		pad();
	}
	translate([-lengthCopler,(widthPad+widthCopler+0.25),0])
	{
		outerLink();
	}
	
}


module turnedTrack(degrees)
{
	$fn = 60;
	rotate([0,degrees,0])
	{
		union()
		{
			translate([-lengthCopler,-widthPad-widthCopler,0])
			{
				outerLink();
			}
			translate([-lengthCopler,0,0])
			{
				innerLink();
			}
			translate([-lengthCopler,widthPad+widthCopler,0])
			{
				outerLink();
			}
		}
	}
	translate([0,-(widthPad+widthCopler)/2,0])
	{
		pad();
	}
	translate([0,(widthPad+widthCopler)/2,0])
	{
		pad();
	}
	
}



module printPad()
{
	translate([0,0,widthPad/2])
		rotate([90,0,0])
			pad();
}

module pad()
{
lockPinWidthPad = lengthPad-thickness;
    difference()
    {
        union()
        {
            hull()
            {
                rotate([90,0,0])
                {
                    cylinder(widthPad,thickness/2,thickness/2,true);
                }
                translate([lockPinWidthPad,0,0])
                {
                    rotate([90,0,0])
                    {
                        cylinder(widthPad,thickness/2,thickness/2,true);
                    }
                }
            }
            space = lockPinWidthPad/3;
			widthProfile = 1;
			thicknessProfile = 1;
			
            for(i=[0.5:1:3])//here something changing to make it fit
            {
                translate([i*space,0,-thickness/2])
                {
                    hull()
                    {
                        translate([0,widthPad/2-1,0])
                        {
                            cylinder(thicknessProfile,widthProfile,widthProfile, true);
                        }
                        translate([0,-widthPad/2+1,0])
                        {
                            cylinder(thicknessProfile,widthProfile,widthProfile, true);
                        }
                    }
                }
            }
        }
        rotate([90,0,0]) cylinder(20,holeS,holeS,true);
        translate([lockPinWidthPad,0,0])
        {
            rotate([90,0,0]) cylinder(20,holeS,holeS,true);
        }
    }
}

module printOuterLink()
{
	translate([0,0,widthCopler/2])
		rotate([90,0,0])
			outerLink();
}

module outerLink()
{
    difference()
    {
        hull()
        {
            rotate([90,0,0])
			{
				cylinder(widthCopler,thickness/2,thickness/2,true);
			}
            translate([lengthCopler,0,0])
            {
                rotate([90,0,0])
                {
                    cylinder(widthCopler,thickness/2,thickness/2,true);
                }
            }
        }
        
        rotate([90,0,0]) cylinder(10,holeS,holeS,true);
        translate([lengthCopler,0,0])
        {
            rotate([90,0,0]) cylinder(10,holeS,holeS,true);
        }
    }

}

module printInnerLink()
{
	translate([0,0,widthCopler/2])
		rotate([90,0,0])
			innerLink();	
}

module innerLink()
{
	pinhight = 7;
	outerLink();
    difference()
    {
        translate([lengthCopler/2,0,0])
        {
            hull()
            {
                translate([0,0,pinhight])		//hight = y-1
                {
                    rotate([0,90,0])
                    {
						hull()
						{
							translate([0,0,-lengthCopler/8])
								sphere(widthCopler/4);	
							translate([0,0,lengthCopler/8])
								sphere(widthCopler/4);	
						}
                        //cylinder(lengthCopler/2,widthCopler/4,widthCopler/4,true);
                    }
                }
                rotate([0,90,0])
                {
                    cylinder(lengthCopler-1,widthCopler/2,widthCopler/2,true);
                }
            }
        }
        rotate([90,0,0]) cylinder(10,holeS,holeS,true);
        translate([lengthCopler,0,0])
        {
            rotate([90,0,0]) cylinder(10,holeS,holeS,true);
        }
    }
}