$fn = 30;

all();

module all()	
{
	sideWall();
}

module sideWall()
{
	thickness = 3;
	difference()
	{
		union()
		{
			wallshape(thickness);
			for(i=[0:1:4])
			{
				position = 90+(47*i);
				translate([position,8,thickness])
				{
					shaftextrucion();
				}
			}
		}
		for(i=[0:1:4])
		{
			position = 90+(47*i);
			translate([position,8,0])
			{
				cylinder(50,1.5,1.5,true);
			}
		}
	}
}


module wallshape(thickness)
{
	linear_extrude(thickness)
	{	
		hight = 65;
		points = [[50,0], [290,0], [320,35], [310,50], [250,hight], [0,hight],[0,50],[25,10]];
		polygon(points);
	}
}


module shaftextrucion()
{
	cylinder(2.5,5,5);
	cylinder(4.5,4,4);

}
//id = 4
//od = 6


/*
	hight = 65;
	points = [[50,0], [290,0], [320,35], [310,50], [250,hight], [0,hight],[0,50],[25,10]];
	polygon(points);
*/
/*thickness = 3;
	hight = 65;
	points = [[50,0,0], [290,0,0], [320,0,35], [310,0,50], [250,0,h	ight], [0,0,hight],[0,0,50],[25,0,10],
              [50,thickness,0], [290,thickness,0], [320,thickness,35], [310,thickness,50], 
			  [250,thickness,hight], [0,thickness,hight], [0,thickness,50],[25,thickness,10]];
    faces =  [[0,1,2,3,4,5,6,7],[8,9,10,11,12,13,14,15],
              [0,1,9,8],[1,2,10,9],[2,3,11,10],[3,4,12,11],[4,5,13,12],[5,6,14,13],[6,7,15,14],[7,0,8,15]];
    polyhedron(points,faces,10);
*/