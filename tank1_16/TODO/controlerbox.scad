$fn = 30;

edge = 2;
wall = 3;

translate([0,100,0])
{
	top();
}
translate([0,0,15])
{	
	bottom();
}
module bottom()
{
	difference()
	{	
		union()
		{	
			difference()
			{
				minkowski()
				{
					cube([140-edge*2, 70-edge*2, 30-edge*2], true);
					sphere(edge);
				}
				translate([0,0,wall])
				{
					cube([140-edge*2-wall, 70-edge*2-wall, 30-edge*2+wall], true);
				}	
			}
			translate([(140-edge*2-wall)/2-5, (70-edge*2-wall)/2-5,0])
			{
				cylinder(29,3,3,true);//screwposts
				cylinder(29,5,0,true);
			}
			translate([(140-edge*2-wall)/2-5, -(70-edge*2-wall)/2+5,0])
			{
				cylinder(29,3,3,true);//screwposts
				cylinder(29,5,0,true);
			}
			translate([-((140-edge*2-wall)/2-5), (70-edge*2-wall)/2-5,0])
			{
				cylinder(29,3,3,true);//screwposts
				cylinder(29,5,0,true);
			}
			translate([-((140-edge*2-wall)/2-5), -(70-edge*2-wall)/2+5,0])
			{
				cylinder(29,3,3,true);//screwposts
				cylinder(29,5,0,true);
			}
			translate([0,-8,0])
			{
				cylinder(29,3,3,true);//screwposts
				cylinder(29,5,0,true);
			}
		}
		translate([(140-edge*2-wall)/2-5, (70-edge*2-wall)/2-5,0])
		{
			cylinder(30,1.5,1.5,true);//screwholes
		}
		translate([(140-edge*2-wall)/2-5, -(70-edge*2-wall)/2+5,0])
		{
			cylinder(30,1.5,1.5,true);//screwholes
		}
		translate([-((140-edge*2-wall)/2-5), (70-edge*2-wall)/2-5,0])
		{
			cylinder(30,1.5,1.5,true);//screwholes
		}
		translate([-((140-edge*2-wall)/2-5), -(70-edge*2-wall)/2+5,0])
		{
			cylinder(30,1.5,1.5,true);//screwholes
		}
		translate([0,-8,0])
		{
			cylinder(30,1.5,1.5,true);//screwholes
		}
		translate([0,0,14.5])
		{
			cube([140-edge*2, 70-edge*2, 2], true);
		}
		translate([66,7,-11.5])
		{
			rotate([90,0,90])
				#usbSlot();
		}
	}
}

module top()
{
	linear_extrude(1)
	{
		topDeck();
	}
}

module topDeck()
{
	mirror([1,0,0])
	{
		controlerTopHalf();		
	}
	controlerTopHalf();
}

module controlerTopHalf()
{	
	difference()	
	{
		translate([(140-edge*2)/4,0])
		{
			square([(140-edge*2)/2, (70-edge*2)],true);
		}
		translate([45,-5,0])
		{
			controlerJoystickHoles();	
		}
		translate([6,17,0])
		{
			square([12,9], true);//display
		}
		translate([7.5,-2,0])
		{
			circle(3.5);//button
		}
		translate([22.5,-2,0])
		{
			circle(3.5);//button
		}
		translate([30,18,0])
		{
			circle(3);//switch
		}
		translate([50,18,0])
		{
			circle(3);//switch
		}
		translate([(140-edge*2-wall)/2-5, (70-edge*2-wall)/2-5,0])
		{
			circle(1.5);//screwholes
		}
		translate([(140-edge*2-wall)/2-5, -(70-edge*2-wall)/2+5,0])
		{
			circle(1.5);//screwholes
		}
		translate([0,-8,0])
		{
			circle(1.5);//screwholes
		}
	}
}

module controlerJoystickHoles()
{
	circle(10);//joystick fole
	translate([10.25,13.1,0])
	{
		circle(1.5);
	}
	translate([10.25,-13.1,0])
	{
		circle(1.5);
	}
	translate([-10.25,-13.1,0])
	{
		circle(1.5);
	}
	translate([-10.25,13.1,0])
	{
		circle(1.5);
	}
}
module usbSlot()
{
	points = [[1.5,0],[6.35,0], [7.85,1.5], [7.85,2.8], [0,2.8], [0,1.5]];
	linear_extrude(10)
		polygon(points);
}









