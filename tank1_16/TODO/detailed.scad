        //hull();
//rotate([0,240,0]){rearPlate();}
sidePlate();   

module hull()
{
    translate([30,0,0]){bottomPlate();}
    translate([0,0,70]){topPlate();} 
}

module bottomPlate()
{
    cube([260,90,4.55]);
}

module topPlate()
{
    cube([250,90,3]);
}

module sidePlate()
{
    sideThickness = 3;
    points =    [[30,0,0], [290,0,0], [320,0,35], [310,0,50], 
                [250,0,70], [0,0,70],                       //outer side
                [30,sideThickness,0], [290,sideThickness,0],  //inner side
                [320,sideThickness,35], [310,sideThickness,50], 
                [250,sideThickness,70], [0,sideThickness,70]]; 
    faces =     [[0,1,2,3,4,5],[6,7,8,9,10,11],             //side faces
                [0,1,7,6],[1,2,8,7],[2,3,9,8],              //edge faces
                [3,4,10,9],[4,5,11,10],[5,0,6,11]];        
    polyhedron(points,faces);
}

module rearPlate()
{
    cube([76,90,4.55]);
}  