$fn = 60;

armLlength = 50;
armWidth = 10;
armThickness = 7;

totalLengthShaft = 34;
thickShaftLength = totalLengthShaft - 18;
ShaftR = 8;
pinR = 0.75;
boltLegnth = 25;
nutR = 3.7;
boltOR = 1.5;

bearingIR = 3.9;
bearingThickness = 3.5;
bearingSpacerR = 5;
bearingSpacerThickness = 4;
endCapHight = 7.5;
shaftLegnth = 30;

bearingMountOR = 10;
bearingMountIR = 6.2;
bearingMountHightH = 4;
bearingMountHightL = bearingMountHightH-(bearingThickness/2);

bearingCapThickness = 2;

springMountPossition = 30;//from the hull
springNTwists = 7;
springLeafThickness = 1.2;
springSpacing = 3;
springWidth = 5;	
springThickness = armThickness;
springMountingBallR = armWidth/3;

//spring(10);

printSuspensionArm();


module printSuspensionArm()
{
	suspensionArm();
	translate([armLlength+20,0,0])
		endCapShaft();
	translate([0,bearingMountOR+10,0])
		rotate([0,0,-90])
			spring(springNTwists);
	
	//translate([armLlength+40,0,0])
		//mount();
	//translate([armLlength+40,bearingMountOR+10,0])
		//mount();
		
}

module mount()
{
	rotate_extrude()
	{
		translate([boltOR,0,0])
		{
			square([bearingIR-boltOR,bearingMountHightH]);
			square([bearingIR+1-boltOR,bearingMountHightL]);
		}
	}
}

module suspensionArm()
{
	difference()
	{	
		union()
		{
			linear_extrude(armThickness)
				difference()
				{
					union()
					{
						circle(ShaftR,true);//rounded end arm
						
						translate([armLlength/2,0,0])		//arm
							square([armLlength,armWidth],true);
						
						translate([armLlength,0,0])		//bearing end arm
							circle(bearingMountOR,true);
						
					}
					translate([armLlength-springMountPossition,armWidth/3,0])
						circle(springMountingBallR+0.1);
					
					translate([armLlength,0,0])		//bearing end arm
						circle(bearingMountIR,true);
				}
			translate([0,0,armThickness])	
				shaft();
			}
		cylinder(totalLengthShaft-boltLegnth+armThickness,nutR,nutR,$fn=6);
		for(i=[0:60:360])
		{
			rotate([0,0,i])
				translate([ShaftR-2,0,-0.1])
					cylinder(totalLengthShaft,pinR,pinR);
		}
	}
}


/*

bearingIR = 4;
bearingSpacerR = 5.5;
bearingSpacerThickness = 4;
boltOR = 1.5;
endCapHight = 7.5;
shaftLegnth = 30;
*/

module shaft()
{
	//taperd end
	translate([0,0,totalLengthShaft-endCapHight])
	{
		difference()
		{
			cylinder(bearingIR,bearingIR,0);
			cylinder(totalLengthShaft,boltOR,boltOR);
		}
	}
	//shaft
	rotate_extrude()
	{
		difference()
		{
			union()
			{
				square([bearingIR,totalLengthShaft-endCapHight]);
				square([ShaftR,thickShaftLength]);
			}
			square([boltOR,totalLengthShaft]);
		}
	}
}

module endCapShaft()
{
	difference()
	{
		rotate_extrude()
		{
			difference()
			{
				union()
				{
					square([bearingIR,endCapHight]);
					square([bearingSpacerR,bearingSpacerThickness]);
				}
				square([boltOR,endCapHight]);
			}
		}
		cylinder(bearingIR,bearingIR,0);
	}
}

module spring(NTwists)
{
	linear_extrude(springThickness)
	{
		circle(springMountingBallR);
		hull()
		{
			translate([0,springMountingBallR/5*3,0])
				square([springLeafThickness,springMountingBallR]);
			rotate([0,0,39])
				translate([0,springMountingBallR/5*3,0])
					square([springLeafThickness,springMountingBallR]);
		}
		//translate([0,springMountingBallR,0])
			//circle(springMountingBallR/2+0.2);
	}
	springSectionWidth = springSpacing+springLeafThickness;
	translate([-springWidth/2,springMountingBallR*2,0])	
		for(i=[0:1:NTwists])
		{
			translate([0,i*2*springSectionWidth,0])
				springSection();
			translate([springWidth,(i*2+1)*springSectionWidth,0])
				mirror([1,0,0])
					springSection();
		}
}

module springSection()
{
	springOR = springSpacing/2+springLeafThickness;
	springIR = springSpacing/2;
	linear_extrude(springThickness)
		difference()
		{
			union()
			{
				circle(springOR,true);
				translate([springWidth/4,0,0])
					square([springWidth/2,springOR*2],true);
			}
			union()
			{
				circle(springIR,true);
				translate([springWidth/2,0,0])
					square([springWidth,springIR*2],true);
			}
		}
}

