	include <hardware/servo.scad>

$fn = 60;

totalWidth = 100;
//frontAsseblyDisplayed();
print();

module print()
{
	frontPlateMount();
	translate([0,-15,0])
		lamp();
	translate([0,-40	,0])
		lamp();
	
	translate([-100,-40,0])
	!frontSideParts();
	translate([-100,140,0])	
		mirror([0,1,0]) frontSideParts();
}
module frontAsseblyDisplayed()
{
	rotation = 0;
	frontPlateMount();
	
	translate([28,25,69.6])
		rotate([90,rotation,0])
			#lamp();
	translate([28,95,69.6])
		rotate([90,rotation,0])
			#lamp();
	
}

module frontPlateMount()
{
	difference()
	{
		translate([0,totalWidth,0])
			rotate([90,0,0])
				linear_extrude(totalWidth)
					tankSub2D();
		
		translate([0,totalWidth/2,0])//servo spacing
		{
			spacing = 10;
			servoHight = 51;
			servoDepth = 19;

			translate([servoDepth+0.1,spacing,servoHight])
				rotate([0,180,0])
					servoMountHole(270);
			translate([servoDepth+0.1,-spacing,servoHight])
				mirror([0,1,0])
					rotate([0,180,0])
						servoMountHole(270);
		}
		translate([23,0,38])//tentioning mounts
			rotate([0,90,0])
			{
				boltHole = 1.75;
				headHole = 3.5;
				nutHole = 3.5;
				translate([0,10,0])//boltholders
				{
					translate([0,0,-22])
						cylinder(35,boltHole,boltHole);
					translate([0,0,9])
						cylinder(10,headHole,headHole);
				}
				translate([0,90,0])
				{
					translate([0,0,-22])
						cylinder(35,boltHole,boltHole);
					translate([0,0,9])
						cylinder(10,headHole,headHole);
				}
				translate([0,40,0])//nutholders
				{
					translate([0,0,-10])
						cylinder(35,boltHole,boltHole);
					translate([0,0,7])
						cylinder(12,headHole,headHole);
					translate([0,0,-1])
						cylinder(5,nutHole,nutHole, $fn = 6);
				}
				translate([0,60,0])//nutholders
				{
					translate([0,0,-10])
						cylinder(35,boltHole,boltHole);
					translate([0,0,7])
						cylinder(12,headHole,headHole);
					translate([0,0,-1])
						cylinder(5,nutHole,nutHole, $fn = 6);
				}
			}
		
		rotate([0,41.65-90,0])//lower plate mount holes
		{
			boltHole = 1.75;
			nutHole = 3.5;
			translate([22,10,-20])
			{
				cylinder(50,boltHole,boltHole);
				translate([0,0,30])
					cylinder(15	,nutHole,nutHole, $fn = 6);
			}
			translate([22,50,-20])
			{
				cylinder(50,boltHole,boltHole);
				translate([0,0,30])
					cylinder(15	,nutHole,nutHole, $fn = 6);
			}
			translate([22,90,-20])
			{
				cylinder(50,boltHole,boltHole);
				translate([0,0,30])
					cylinder(15	,nutHole,nutHole, $fn = 6);
			}
		}
		rotate([0,-31+90,0]) //upper plate mount holes
		{
			boltHole = 1.75;
			nutHole = 3.5;
			translate([-22,90,54])
			{
				cylinder(20,boltHole,boltHole);
				cylinder(5,nutHole,nutHole, $fn = 6);
			}
			translate([-37,50,40])
			{
				cylinder(25,boltHole,boltHole);
				cylinder(15,nutHole,nutHole, $fn = 6);
			}
			translate([-22,10,54])
			{
				cylinder(20,boltHole,boltHole);
				cylinder(5,nutHole,nutHole, $fn = 6);
			}
		}
		
		translate([28,-0.10,69.6])//pin for headlights
		{
			rotate([-90,0,0])
				cylinder(100.2,1.1,1.1);
		}
		
		translate([-45,0,75]) //hinge holes back
		{
			translate([0,30,0])
				cube([10,20,10],true);
			translate([0,70,0])
				cube([10,20,10],true);
		}
		
		translate([28,25,69.6]) // lampholes
			minkowski()
			{ 
				union()
				{
					hull()
					{
						rotate([90,65,0])
							lamp();
						rotate([90,30,0])
							lamp();
						rotate([90,0,0])
							lamp();
					}
					translate([0,70,0])
						hull()
						{
							rotate([90,65,0])
								lamp();
							rotate([90,30,0])
								lamp();
							rotate([90,0,0])
								lamp();
						}
				}
				sphere(0.5);
			}
	}
}

module frontSideParts()
{
	interWallThickness = 8;
	wallThickness = 2.5;
	fenderWidth = 50;
	difference()
	{
		union()
		{
			linear_extrude(interWallThickness)
				difference()
				{
					tankSub2D(false);
					axelX = 2.5;
					axelY = 14;
					axelD = 11;
					hull()
					{
						translate([axelX,axelY,0])
							circle(axelD);
						circle(1);
					}
				}
			
			#translate([0,0,interWallThickness])
				linear_extrude(wallThickness+fenderWidth)
					intersection()
					{
						tankSub2D(false);
						translate([-50,70,0])
							square([70,20]);
					}
		}
		translate([5,68,interWallThickness+0.01])//top nut hole
			cylinder(15,4,4);
		
		translate([-45,75,wallThickness+interWallThickness])
		{
			translate([0,0,-wallThickness/2])
				cube([10,10,wallThickness],true);
			translate([0,0,15])
				cube([10,10,10],true);
			translate([0,0,30])
				linear_extrude(25)
					polygon([[-5,-5],[5,-5],[5, 1.7],[-5,3.4]]);
		}
		
	}
}
module servoMountHole(var1)
{
	rotate([90,180,180])
		#servoHole(var1);
	translate([0,30,0])
		cube([35,35,15],true);
	translate([-30,20,-18])
		cube([50,15,13]);
	translate([0,-12,-10])
		cube([20,15,15]);
}

module tankSub2D(wallInserts = true)
{
	plateThickness = 4.75;
	hullPoints = [[375,0], [435,0], [475,45], [460,70], [375,85]];
	
	intersection()
	{
		forwardshirt = -425;
		translate([forwardshirt,0])
			difference()
			{
				polygon(hullPoints);
				if(wallInserts)
				{
					hull()	//bottom plate
					{
						translate(hullPoints[0])
							circle(plateThickness);
						translate(hullPoints[1])
							circle(plateThickness);
					}
					hull()	//lowerglacias
					{
						translate(hullPoints[1])
							circle(plateThickness);
						translate(hullPoints[2])
							circle(plateThickness);
					}
					hull()	//uperglacias
					{
						translate(hullPoints[2])
							circle(plateThickness);
						translate(hullPoints[3])
							rotate(31)
								square(plateThickness*2, true);
					}
				}
				hight = 38;
				depthPlacement = 10;
				axelOD = 4.5;
				length = 10;	
				
				translate([depthPlacement-forwardshirt,hight])
					hull()
					{
						circle(axelOD);
						translate([length,0]) 
							circle(axelOD);
					}
				boltID = 1.75;
				translate([-forwardshirt,0])
				{
					translate([20,29])
						circle(boltID);
					translate([5,68])
						circle(boltID);
					translate([-45,75])
						circle(boltID);
				}
			}
		union()
		{
			//hull part
			square([100,85]);
			polygon([[0,70],[-60,80],[-60,100],[0,100]]);
			//mount eye on rear
			hull()
			{
				translate([-45,75])
					circle(5);
				translate([-45,80])
					circle(5);
			}
		}
	}
}

module lamp()
{	
	widthLamp = 20;
	difference()
	{
		linear_extrude(widthLamp)//cap
		{
			difference()
			{
				union()
				{
					intersection()
					{
						headLightShape();
						polygon([[0,0],[10,0],[10,-6],[0,-1]]);
					}
					hull()
					{	
						rotate([0,0,65])	
							headLightShape();	
							translate([7.1,-1])
								circle(0.1);
					}
				}
				circle(1.1);
			}
		}
		translate([2,-1,2])//LED space
		{
			linear_extrude(16)
				polygon([[0,0],[1,6],[6,6],[6,0]]);
			translate([0,0,3])
			linear_extrude(10)
				polygon([[0,-5],[1,6],[5,6],[5,-5]]);
		}
	}
	translate([8,-5,9])//pinholder
	{
		difference()
		{
			union()
			{
				translate([-1.6,0,0])
					cube([3.2,1,2]);
				cylinder(2,1.6,1.6);
			}
			translate([0,0,-0.1])
				cylinder(2.5,1,1);
		}
	}
}
module headLightShape()
{
	intersection()
	{
		hull()
		{
			circle(1.6);
			polygon([[-1,-4],[10,-4],[10,1]]);
		}
		translate([-28,-69.6])
			tankSub2D(false);
	}
}
//todo:
//make file with arm and wire route